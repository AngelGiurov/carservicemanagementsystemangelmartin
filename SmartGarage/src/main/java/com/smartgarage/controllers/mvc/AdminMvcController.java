package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.AuthenticationFailureException;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.*;
import com.smartgarage.models.dtos.*;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.models.mappers.*;
import com.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@Controller
@RequestMapping("/admin-panel")
public class AdminMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ServicesService servicesService;
    private final VehicleService vehicleService;
    private final ModelMapper modelMapper;
    private final ModelService modelService;
    private final MakeService makeService;
    private final VehicleMapper vehicleMapper;
    private final VisitMapper visitMapper;
    private final VisitService visitService;

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("vehicleMake")
    public List<Make> populateVehicleMakes(HttpSession session) {
        return makeService.getAll();
    }

    @ModelAttribute("vehicleModel")
    public List<com.smartgarage.models.Model> populateVehicleModels(HttpSession session) {
        return modelService.getAll();
    }

    @ModelAttribute("allVehicles")
    public List<Vehicle> populateVehicles(HttpSession session) {
        return vehicleService.getAll();
    }




    @Autowired
    public AdminMvcController(UserService userService,
                              UserMapper userMapper,
                              AuthenticationHelper authenticationHelper, ServicesService servicesService, VehicleService vehicleService, ModelMapper modelMapper, ModelService modelService, MakeService makeService, VehicleMapper vehicleMapper, ServiceMapper serviceMapper, VisitMapper visitMapper, VisitService visitService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.servicesService = servicesService;
        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
        this.makeService = makeService;
        this.vehicleMapper = vehicleMapper;
        this.visitMapper = visitMapper;
        this.visitService = visitService;
    }

    @GetMapping
    public String adminPanel(Model model, HttpSession session) {

        try {
            User currentUser = userService.getById(1);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("services", servicesService.getServicesByUser(currentUser));
            model.addAttribute("vehicles", vehicleService.getByOwner(currentUser.getId()));
            model.addAttribute("modelDto", new ModelDTO());
            model.addAttribute("brandDto", new BrandDTO());
            model.addAttribute("vehicleDto", new VehicleDTO());
            model.addAttribute("vehicle", new EditVehicleDto());
            model.addAttribute("service", new Service());
            model.addAttribute("visitDto",new VisitDTO());
            if (currentUser.getRole() == Roles.CUSTOMER) {
                return "redirect:/unauthorized";
            }
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("registerDto", new RegisterDto());
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }
        return "admin-panel";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto,
                                 Model model,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "admin-panel";
        }
        User authorizedUser;
        try {
            authorizedUser = userService.getById(2);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User userToBeRegistered = userMapper.fromDto(registerDto);
            model.addAttribute("currentUser", userToBeRegistered);
            userService.registerUser(authorizedUser, userToBeRegistered);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "redirect:/admin-panel";
        }
        return "admin-panel";
    }

    @PostMapping("/model")
    public String registerModel(@Valid @ModelAttribute("modelDto")
                                            ModelDTO modelDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = userService.getById(1);
            com.smartgarage.models.Model modelToCreate = modelMapper.fromDto(modelDto);
            model.addAttribute("currentUser", userToVerify);
            model.addAttribute("registerDto", new RegisterDto());
            model.addAttribute("brandDto", new BrandDTO());
            modelService.createModel(userToVerify, modelToCreate);

        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("make_id", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "admin-panel";
    }

    @PostMapping("/make")
    public String registerMake(@Valid @ModelAttribute("brandDto")
                                           BrandDTO brandDTO,
                               BindingResult bindingResult,
                               Model model,
                               HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = userService.getById(1);
            model.addAttribute("currentUser", userToVerify);
            model.addAttribute("registerDto", new RegisterDto());
            model.addAttribute("modelDto", new ModelDTO());
            makeService.createMake(userToVerify, brandDTO.getName());
        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("model_name", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("make_id", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }
        return "admin-panel";
    }

//    @PostMapping("/vehicle")
//    public String registerVehicle(@Valid @ModelAttribute("vehicleDto")
//                                              VehicleDTO vehicleDto,
//                                  BindingResult bindingResult,
//                                  HttpSession session) throws BindException {
//
//        if (bindingResult.hasErrors()) {
//            return "redirect:/admin-panel";
//        }
//
//        try {
//            User user = userService.getById(1);
//            Vehicle vehicle = vehicleMapper.fromDto(vehicleDto);
//            vehicleService.create(vehicle, user);
//        } catch (DuplicateEntityException e) {
//            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
//            return "redirect:/admin-panel";
//        } catch (UnauthorizedOperationException e) {
//            return "redirect:/notFound";
//        }
//
//        return "redirect:/admin-panel";
//    }

//    @PostMapping("/vehicleUpdate")
//    public String updateVehicle(@Valid
//                                @ModelAttribute("vehicle")
//                                            EditVehicleDto editVehicleDto,
//                                BindingResult bindingResult,
//                                HttpSession session) throws BindException {
//
//        if (bindingResult.hasErrors()) {
//            return "redirect:/admin-panel";
//        }
//
//        try {
//            User user = userService.getById(1);
//            Vehicle vehicleToUpdate = vehicleMapper.fromDto(editVehicleDto, 1);
//            vehicleService.update(user, vehicleToUpdate);
//        } catch (DuplicateEntityException e) {
//            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
//            return "redirect:/admin-panel";
//        } catch (UnauthorizedOperationException e) {
//            return "redirect:/notFound";
//        }
//
//        return "redirect:/admin-panel";
//    }

    @PostMapping("/serviceUpdate")
    public String updateService(@Valid
                                @ModelAttribute("service")
                                        Service service,
                                BindingResult bindingResult,
                                HttpSession session) throws BindException {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User user = userService.getById(1);
            servicesService.update(service, user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
            return "redirect:/admin-panel";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "redirect:/admin-panel";
    }

    @PostMapping("/serviceDelete")
    public String deleteService(@Valid
                                @ModelAttribute("service")
                                        Service service,
                                BindingResult bindingResult,
                                HttpSession session) throws BindException {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User user = userService.getById(1);
            servicesService.delete(service.getId(), user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
            return "redirect:/admin-panel";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "redirect:/admin-panel";
    }

    @GetMapping("/{id}/user")
    public String userPage(@PathVariable int id, Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            User user = userService.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("services", servicesService.getServicesByUser(currentUser));
            model.addAttribute("vehicles", vehicleService.getByOwner(currentUser.getId()));
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("editProfileDto", new EditProfileDto());
        model.addAttribute("visitDto", new VisitDTO());
        return "user";
    }



    @PostMapping("/visit")
    public String handleRegisterVisit(@Valid @ModelAttribute("visitDto")
                                              VisitDTO visitDto,
                                      BindingResult bindingResult,
                                      HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = userService.getById(1);
            Visit visitToCreate = visitMapper.fromDto(visitDto);
            visitService.create(visitToCreate, userToVerify);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "redirect:/admin-panel";
    }



























//    @PostMapping("/{id}/delete")
//    public String userDelete(@PathVariable int id,
//                             Model model,
//                             BindingResult bindingResult,
//                             HttpSession session) {
//        if (bindingResult.hasErrors()) {
//            return "redirect:/user";
//        }
//        User authorizedUser;
//        try {
//            authorizedUser = authenticationHelper.tryGetUser(session);
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/Login";
//        }
//        try {
//            User userToDelete = userService.getById(id);
//            model.addAttribute("editProfileDto", new EditProfileDto());
//            model.addAttribute("filterServicesDto", new FilterServicesDTO());
//            User userToBeUpdated = userMapper.fromDto(editProfileDto, authorizedUser.getId());
//            userService.update(authorizedUser, userToBeUpdated);
//        } catch (AuthenticationFailureException e) {
//
//            bindingResult.rejectValue("username", "auth_error", e.getMessage());
//            return "redirect:/user";
//
//        } catch (UnauthorizedOperationException e) {
//            return "redirect:/notFound";
//        }
//        return "redirect:/user";
//    }

}
