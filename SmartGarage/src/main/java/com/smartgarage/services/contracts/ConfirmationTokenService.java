package com.smartgarage.services.contracts;

import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;

public interface ConfirmationTokenService {
    String create(User userToCreate, int hoursActive);

    ConfirmationToken getToken(String token);

    void setConfirmedAt(ConfirmationToken token);

    void checkIfTokenActive(ConfirmationToken tokenToBeChecked);
}
