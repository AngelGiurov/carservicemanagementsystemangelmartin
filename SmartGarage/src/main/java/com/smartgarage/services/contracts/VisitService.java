package com.smartgarage.services.contracts;

import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;

import java.io.IOException;
import java.util.List;

public interface VisitService {
    Visit getById(int id);

    List<Visit> getAll();

    void create(Visit visit, User user);

    void update(Visit visit, User user);

    void delete(int id, User user);

    Visit addService(Visit visit, Service service, User user);

    void sendReportEmail(int visitId, int currencyId, User user);

    List<Visit> getByUser(User user);

    void currencyConverter() throws IOException;
}
