package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartgarage.models.enums.Roles;

import javax.validation.constraints.*;

import static com.smartgarage.project_constants.Constants.EMAIL_LENGTH_RESTRICTION;
import static com.smartgarage.project_constants.Constants.USER_PASSWORD_PATTERN_INVALID;

public class LoginDto {
    @JsonProperty(value = "email")
    @NotNull
    @Email
    @Size(min = 5, max = 40, message = EMAIL_LENGTH_RESTRICTION)
    private String email;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
            message = USER_PASSWORD_PATTERN_INVALID)
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
