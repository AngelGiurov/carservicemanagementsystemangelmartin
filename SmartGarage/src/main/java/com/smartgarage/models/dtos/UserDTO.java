package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.smartgarage.project_constants.Constants.*;

public class UserDTO {
    @JsonProperty(value = "username")
    @Size(min = 2, max = 20, message = USERNAME_LENGTH_INVALID)
    private String username;

    @JsonProperty(value = "email")
    @NotNull
    @Email
    private String email;

    @JsonProperty(value = "password")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
        message = USER_PASSWORD_PATTERN_INVALID)
    private String password;

    @JsonProperty(value = "phone_number")
    @Size(min = 10,max = 10, message = USER_PHONE_LENGTH_ERROR)
    private String phoneNumber;


    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
