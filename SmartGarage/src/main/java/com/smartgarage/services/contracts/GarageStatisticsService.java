package com.smartgarage.services.contracts;

public interface GarageStatisticsService {

    long getVehicles();

    long getCustomers();

    long getServices();

    long getVisits();
}
