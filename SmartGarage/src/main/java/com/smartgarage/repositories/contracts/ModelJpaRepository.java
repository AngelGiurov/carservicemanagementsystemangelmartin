package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelJpaRepository extends JpaRepository<Model,Integer> {
    Model findById(int id);

    Model findModelByName(String name);
}
