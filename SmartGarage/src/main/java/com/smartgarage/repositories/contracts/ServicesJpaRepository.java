package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ServicesJpaRepository extends JpaRepository<Service, Integer> {

    List<Service> findAll();

    Service findById(int id);

    Service findByName(String name);

    @Query(value = "Select e from Service e where (:price is null or e.price < :price)  " +
            "and (:name is null or e.name = :name)")
    List<Service> filterByPrice(Optional<Double> price, Optional<String> name);

    @Query(value = "select s from Visit v" +
            " join v.services as s" +
            " join v.vehicle as vehicle " +
            " join vehicle.user as user where user.id = :userId")
    List<Service> getServicesByUserId(int userId);

    //Set<Service> findServicesById(int id);

}
