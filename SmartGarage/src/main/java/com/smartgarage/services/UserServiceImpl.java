package com.smartgarage.services;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.ForgottenPasswordResetDTO;
import com.smartgarage.models.dtos.PasswordChangeDto;
import com.smartgarage.models.enums.UserSortOptions;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.ConfirmationTokenJpaRepository;
import com.smartgarage.repositories.contracts.UserRepository;
import com.smartgarage.repositories.contracts.VehicleJpaRepository;
import com.smartgarage.services.contracts.EmailSender;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.helpers.MailBuilder.passwordBackUpMessage;
import static com.smartgarage.helpers.MailBuilder.registrationMessage;
import static com.smartgarage.project_constants.Constants.ONLY_EMPLOYEES_CAN_DELETE_USER_PROFILE;
import static com.smartgarage.project_constants.Constants.USER_NOT_OWNER;


@Service
public class UserServiceImpl implements UserService {

    public static final String USER_REGISTRATION_RESTRICTION = "Only admins can register new users";
    public static final int HOURS_ACTIVE = 24 * 60;
    public static final int HOUR_ACTIVE = 60;
    public static final String ACCOUNT_VERIFICATION = "Account verification";
    public static final String PASSWORD_RESET = "Password Reset";
    public static final String PASSWORDS_DO_NOT_MATCH = "Passwords do not match";
    public static final String UNAUTHORIZED_PASSWORD_CHANGE = "Passwords do not match";
    public static final String UNAUTHORIZED_OPERATION = "Unauthorized operation";
    public static final String OLD_PASSWORD_AND_NEW_PASSWORD_SHOULD_NOT_MATCH = "Old password and new password should not match";
    public static final String CUSTOMER_S_PROFILE_MUST_BE_ENABLED = "Customer's profile must be enabled";
    public static final String UNAUTHORIZED_VEHICLE_ADD = "Only shop employees are allowed to add vehicles";
    private final UserRepository userRepository;
    private final ConfirmationTokenServiceImpl confirmationTokenService;
    private final ConfirmationTokenJpaRepository confirmationTokenJpaRepository;
    private final EmailSender emailSender;
    private final VehicleJpaRepository vehicleJpaRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           ConfirmationTokenServiceImpl confirmationTokenService,
                           ConfirmationTokenJpaRepository confirmationTokenJpaRepository, EmailSender emailSender, VehicleJpaRepository vehicleJpaRepository) {
        this.userRepository = userRepository;
        this.confirmationTokenService = confirmationTokenService;
        this.confirmationTokenJpaRepository = confirmationTokenJpaRepository;
        this.emailSender = emailSender;
        this.vehicleJpaRepository = vehicleJpaRepository;
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            User userToBeCreated = userRepository.findUserByUsername(user.getUsername());
            if (userToBeCreated == null) {
                throw new EntityNotFoundException("", "", "");
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        userRepository.create(user);
    }

    @Override
    public void update(User authorizedUser, User userToBeUpdated) {
        //verifyUserIsOwner(authorizedUser, userToBeUpdated);
        boolean duplicateExists = true;
        try {
            User existingUser = userRepository.findUserByUsername(userToBeUpdated.getUsername());
            if (existingUser.getEmail().equals(userToBeUpdated.getEmail())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", userToBeUpdated.getEmail());
        }
        userRepository.update(userToBeUpdated);
    }

    @Override
    public void delete(int id, User authorizedUser) {
        User userToBeDeleted = getById(id);
        if (authorizedUser.getRole() != Roles.EMPLOYEE) {
            if (!(authorizedUser.getUsername().equals(userToBeDeleted.getUsername()))) {
                throw new UnauthorizedOperationException(ONLY_EMPLOYEES_CAN_DELETE_USER_PROFILE);
            }
        }
        List<Vehicle> userVehicles = vehicleJpaRepository.filterVehicleByOwner(userToBeDeleted.getId());
        for (Vehicle userVehicle : userVehicles) {
            userVehicle.setUser(null);
            vehicleJpaRepository.save(userVehicle);
        }
        List<ConfirmationToken> confirmationToken = confirmationTokenJpaRepository.
                findConfirmationTokensByUser(userToBeDeleted);
        confirmationTokenJpaRepository.deleteAll(confirmationToken);
        userRepository.delete(id);
    }


    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }


    @Override
    public User getByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("", "", "");
        }
        return user;
    }

    @Override
    public User getByEmail(String email) {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            throw new EntityNotFoundException("", "", "");
        }
        return user;
    }


    @Override
    public User getById(int id) {
        return userRepository.findById(id);
    }

    @Override
    public String registerUser(User authorizedUser, User userToBeRegistered) {
        verifyUserIsAuthorized(authorizedUser);
        boolean duplicateExists = true;
        try {
            User userToBeCreated = userRepository.findUserByEmail(userToBeRegistered.getEmail());
            if (userToBeCreated == null) {
                throw new com.smartgarage.exceptions.EntityNotFoundException("", "", "");
            }
        } catch (com.smartgarage.exceptions.EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", userToBeRegistered.getUsername());
        }
        userRepository.create(userToBeRegistered);

        String token = confirmationTokenService.create(userToBeRegistered, HOURS_ACTIVE);
        String message = registrationMessage(userToBeRegistered, token);
        emailSender.send(userToBeRegistered.getEmail(), ACCOUNT_VERIFICATION, message);
        return token;
    }


    @Override
    public void confirmUser(String token) {
        ConfirmationToken confirmationToken = confirmationTokenService.getToken(token);

        confirmationTokenService.checkIfTokenActive(confirmationToken);

        User userToBeEnabled = userRepository.findById(confirmationToken.getUser().getId());
        userToBeEnabled.setEnable(true);
        userRepository.update(userToBeEnabled);

        confirmationToken.setUsedOn(LocalDateTime.now());
        confirmationTokenService.setConfirmedAt(confirmationToken);

    }

    @Override
    public void sendResetPasswordMail(String email) {
        User userToReset = userRepository.findUserByEmail(email);
        String token = confirmationTokenService.create(userToReset, HOUR_ACTIVE);
        String message = passwordBackUpMessage(userToReset, token);

        emailSender.send(email, PASSWORD_RESET, message);
    }

    @Override
    public void resetUserPassword(ConfirmationToken confirmationToken,
                                  ForgottenPasswordResetDTO forgottenPasswordResetDTO) {
        confirmationTokenService.checkIfTokenActive(confirmationToken);

        if (!forgottenPasswordResetDTO.getPassword().equals(forgottenPasswordResetDTO.getRepeatPassword())) {
            throw new EntityNotFoundException("", "", "");
        }

        User userToUpdate = userRepository.findById(confirmationToken.getUser().getId());
        userToUpdate.setPassword(forgottenPasswordResetDTO.getPassword());
        userRepository.update(userToUpdate);

        confirmationToken.setUsedOn(LocalDateTime.now());
        confirmationTokenService.setConfirmedAt(confirmationToken);

    }

    @Override
    public void changePassword(User userToChangePassword, PasswordChangeDto passwordChangeDto) {
        if (!userToChangePassword.getPassword().equals(passwordChangeDto.getOldPassword())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_PASSWORD_CHANGE);
        }
        if (!passwordChangeDto.getNewPassword().equals(passwordChangeDto.getRepeatNewPassword())) {
            throw new UnauthorizedOperationException(PASSWORDS_DO_NOT_MATCH);
        }
        if (passwordChangeDto.getOldPassword().equals(passwordChangeDto.getNewPassword())) {
            throw new UnauthorizedOperationException(OLD_PASSWORD_AND_NEW_PASSWORD_SHOULD_NOT_MATCH);
        }
        User userToUpdate = userRepository.findById(userToChangePassword.getId());
        userToUpdate.setPassword(passwordChangeDto.getNewPassword());
        userRepository.update(userToUpdate);

    }

    @Override
    public void addVehicle(User employee, User customer, Vehicle vehicle) {
        if (!customer.isEnable()) {
            throw new UnauthorizedOperationException(CUSTOMER_S_PROFILE_MUST_BE_ENABLED);
        }
        if (employee.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_VEHICLE_ADD);
        }
        if (vehicle.getUser() != null) {
            throw new DuplicateEntityException("Vehicle", "user", customer.getUsername());
        }

        vehicle.setUser(customer);
        vehicleJpaRepository.save(vehicle);
    }

    @Override
    public List<User> filter(User user,
                             Optional<String> email,
                             Optional<String> username,
                             Optional<String> phoneNumber,
                             Optional<String> make,
                             Optional<String> model,
                             Optional<String> visitStart,
                             Optional<String> visitEnd,
                             Optional<UserSortOptions> postSortOptions) throws ParseException {
        verifyUserIsAuthorized(user);
        return userRepository.filter(email, username, phoneNumber, make, model, visitStart, visitEnd, postSortOptions);
    }

    private void verifyUserIsAuthorized(User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(UNAUTHORIZED_OPERATION);
        }
    }

    public static void verifyUserIsOwner(User authorizedUser, User userToBeUpdated) {
        if (authorizedUser.getRole() != Roles.EMPLOYEE || authorizedUser.getId() != userToBeUpdated.getId()) {
            throw new UnauthorizedOperationException(USER_NOT_OWNER);
        }
    }
}

