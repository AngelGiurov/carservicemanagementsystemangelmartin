package com.smartgarage.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Currency;
import com.smartgarage.models.dtos.CurrencyDto;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.enums.CurrencyENUM;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.CurrencyJpaRepository;
import com.smartgarage.repositories.contracts.ServicesJpaRepository;
import com.smartgarage.repositories.contracts.VisitJpaRepository;
import com.smartgarage.services.contracts.EmailSender;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Set;

@Service
public class VisitServiceImpl implements VisitService {
    public static final String VISIT_CREATE_RESTRICTION = "Only employees can create new visits";
    public static final String VISIT_UPDATE_RESTRICTION = "Only employees can update visit information";
    public static final String VISIT_DELETE_RESTRICTION = "Only employees can delete scheduled visits";
    public static final String VISIT_RULES_VIOLATION = "Only employees can added services to a visit";
    public static final String ONLY_VISIT_OWNERS_CAN_REQUEST_REPORT = "Only visit owners can request report";
    public static final String REPORT_MESSAGE = "Hello %s,%n" +
            "The services done to your vehicle:%s %s " +
            "with license plate %s are: %s%n" +
            "total price for your visit is: %.2f %s%n" +
            "Best regards: Smart garage team";
    public static final String VISIT_REPORT = "Visit Report";
    public static final String REPORT_ERROR = "Only customers from the given Visit and employees can request report!";

    private final VisitJpaRepository visitJpaRepository;
    private final ServicesJpaRepository servicesJpaRepository;
    private final EmailSender emailSender;
    private final CurrencyJpaRepository currencyJpaRepository;

    @Autowired
    public VisitServiceImpl(VisitJpaRepository visitJpaRepository, ServicesJpaRepository servicesJpaRepository, EmailSender emailSender, CurrencyJpaRepository currencyJpaRepository) throws IOException {
        this.visitJpaRepository = visitJpaRepository;
        this.servicesJpaRepository = servicesJpaRepository;
        this.emailSender = emailSender;
        this.currencyJpaRepository = currencyJpaRepository;
    }

    @Override
    public Visit getById(int id) {
        return visitJpaRepository.findById(id);
    }

    @Override
    public List<Visit> getAll() {
        return visitJpaRepository.findAll();
    }

    @Override
    public void create(Visit visit, User user) {
        verifyUserIsAuthorized(user);
        visitJpaRepository.save(visit);
    }

    @Override
    public void update(Visit visit, User user) {
        verifyUserIsAuthorized(user);
        visitJpaRepository.save(visit);
    }

    @Override
    public void delete(int id, User user) {
        verifyUserIsAuthorized(user);
        Visit visit = visitJpaRepository.findById(id);
        if (visit == null) {
            throw new EntityNotFoundException("", "", "");
        }
        visitJpaRepository.delete(visit);
    }

    @Override
    public Visit addService(Visit visit, com.smartgarage.models.Service service, User user) {
        verifyUserIsAuthorized(user);
        com.smartgarage.models.Service existingService = new com.smartgarage.models.Service();

        existingService = servicesJpaRepository.findByName(service.getName());
        if (existingService == null){
            throw new EntityNotFoundException("","","");
        }

        if (visit.getServices().contains(service)) {
            throw new DuplicateEntityException("Service", "name", service.getName());
        }
        visit.getServices().add(existingService);
        visitJpaRepository.save(visit);
        return visit;
    }

    @Override
    public void sendReportEmail(int visitId, int currencyId, User user) {
        Visit visit = getById(visitId);
        User userFromVisit = visit.getVehicle().getUser();
//        if (user.getId() != userFromVisit.getId() || user.getRole()!=Roles.EMPLOYEE){
//            throw new UnauthorizedOperationException(REPORT_ERROR);
//        }
        Set<com.smartgarage.models.Service> services = visit.getServices();
        String message = buildVisitReport(services, visit ,userFromVisit, currencyId);
        emailSender.send(userFromVisit.getEmail(), VISIT_REPORT, message);
    }

    @Override
    public List<Visit> getByUser(User user){
        return visitJpaRepository.findByVehicleUser(user);
    }

    private void verifyUserIsAuthorized(User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(VISIT_CREATE_RESTRICTION);
        }
    }

    private String buildVisitReport(Set<com.smartgarage.models.Service> services, Visit visit, User user, int currencyId) {
        StringBuilder allServices = new StringBuilder();
        Currency currency = currencyJpaRepository.findById(currencyId).orElseThrow(() -> new EntityNotFoundException("","",""));
        double totalPrice = 0;
        for (com.smartgarage.models.Service service : services) {
            allServices.append(service.getName()).append(", ");
            totalPrice+=service.getPrice();
        }
        totalPrice/=currency.getCurrentValue();
        return String.format(REPORT_MESSAGE,
                user.getUsername(),
                visit.getVehicle().getModel().getMake().getName(),
                visit.getVehicle().getModel().getName(),
                visit.getVehicle().getLicensePlate(),
                allServices,
                totalPrice,
                currency.getName());

    }
    @Scheduled(cron = "0 0 12 * * ?")
    @Override
    public void currencyConverter() throws IOException {
        for (CurrencyENUM currency : CurrencyENUM.values()) {
            Currency newCurrency = currencyJpaRepository.findByName(currency.name());
            String url = "https://free.currconv.com/api/v7/convert?q=" + currency.toString() + "_BGN&compact=ultra&apiKey=d5db18d4618737104ded";
            URL obj = new URL(url);
            ObjectMapper objectMapper = new ObjectMapper();
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            StringBuilder response = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            if (responseCode == 200) {
                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String strCurrentLine;
                while ((strCurrentLine = reader.readLine()) != null) {
                    response.append(strCurrentLine);
                }
            }

            response = new StringBuilder(response.toString().replace(currency.toString() + "_BGN", "value"));
            CurrencyDto convertedCurrency = objectMapper.readValue(response.toString(), CurrencyDto.class);
            newCurrency.setCurrentValue(convertedCurrency.getValue());
            currencyJpaRepository.save(newCurrency);
        }
    }
}
