package com.smartgarage.models.mappers;

import com.smartgarage.models.Service;
import com.smartgarage.models.dtos.ServiceDTO;
import com.smartgarage.services.contracts.ServicesService;
import org.springframework.stereotype.Component;

@Component
public class ServiceMapper {

    private final ServicesService servicesService;

    public ServiceMapper(ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    public Service fromDto(ServiceDTO serviceDTO) {
        Service service = servicesService.getByName(serviceDTO.getType());
        dtoToObject(serviceDTO, service);
        return service;
    }

    public Service fromDto(ServiceDTO serviceDTO, int id) {
        Service service = servicesService.getById(id);
        dtoToObject(serviceDTO, service);
        return service;
    }

    private void dtoToObject(ServiceDTO serviceDTO, Service service) {
        service.setName(serviceDTO.getType());
        service.setPrice(serviceDTO.getPrice());
    }

}
