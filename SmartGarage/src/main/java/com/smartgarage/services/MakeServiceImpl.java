package com.smartgarage.services;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Make;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.MakeJpaRepository;
import com.smartgarage.services.contracts.MakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MakeServiceImpl implements MakeService {

    public static final String UNAUTHORIZED_VEHICLE_CREATION = "Only employees can create vehicles";
    public static final String UNAUTHORIZED_MAKE_UPDATE = "Only employees can update make";
    public static final String MAKE = "Make";
    public static final String NAME = "name";
    private final MakeJpaRepository makeJpaRepository;

    @Autowired
    public MakeServiceImpl(MakeJpaRepository makeJpaRepository) {
        this.makeJpaRepository = makeJpaRepository;
    }

    @Override
    public Make getById(int id) {
        return makeJpaRepository.findById(id);
    }

    @Override
    public void createMake(User user, String makeName) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_VEHICLE_CREATION);
        }
        Make make = new Make();
        make.setName(makeName);
        boolean duplicateExists = true;
        try {
            Make makeToBeCreated = makeJpaRepository.findMakeByName(make.getName());
            if (makeToBeCreated == null) {
                throw new EntityNotFoundException("", "", "");
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(MAKE, NAME, make.getName());
        }
        makeJpaRepository.save(make);
    }

    @Override
    public void updateMake(User user, Make make) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_MAKE_UPDATE);
        }
        makeJpaRepository.save(make);
    }

    @Override
    public List<Make> getAll() {
        return makeJpaRepository.findAll();
    }
}
