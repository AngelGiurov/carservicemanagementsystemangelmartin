package com.smartgarage.models.dtos;

import javax.validation.constraints.Pattern;

import static com.smartgarage.project_constants.Constants.USER_PASSWORD_PATTERN_INVALID;

public class ForgottenPasswordResetDTO {

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
            message = USER_PASSWORD_PATTERN_INVALID)
    private String password;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
            message = USER_PASSWORD_PATTERN_INVALID)
    private String repeatPassword;

    public ForgottenPasswordResetDTO() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String resetPassword) {
        this.repeatPassword = resetPassword;
    }
}
