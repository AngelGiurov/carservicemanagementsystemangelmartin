package com.smartgarage.helpers;

import com.posadskiy.currencyconverter.config.ConfigBuilder;

public class CurrencyConverter {

    public static final String CURRENCY_CONVERTER_API_API_KEY = "d5db18d4618737104ded";

    com.posadskiy.currencyconverter.CurrencyConverter converter =
            new com.posadskiy.currencyconverter.CurrencyConverter(new ConfigBuilder()
            .currencyConverterApiApiKey(CURRENCY_CONVERTER_API_API_KEY)
                    .build()
    );

}
