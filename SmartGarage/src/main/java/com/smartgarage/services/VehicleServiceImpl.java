package com.smartgarage.services;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.InvalidLicensePlateException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.Visit;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.ServicesJpaRepository;
import com.smartgarage.repositories.contracts.VehicleJpaRepository;
import com.smartgarage.repositories.contracts.VisitJpaRepository;
import com.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.smartgarage.helpers.*;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {
    public static final String VEHICLE_UPDATE_ERROR = "Regular customers cannot update vehicle info!";
    public static final String UNAUTHORIZED_VEHICLE_CREATION = "Only employees can create vehicles";
    public static final String UNSUPPORTED_VEHICLE_ADD = "Vehicles who are already owned cannot have another owners";
    private final VehicleJpaRepository vehicleJpaRepository;
    private final VisitJpaRepository visitJpaRepository;
    private final ServicesJpaRepository servicesJpaRepository;


    @Autowired
    public VehicleServiceImpl(VehicleJpaRepository vehicleJpaRepository, VisitJpaRepository visitJpaRepository, ServicesJpaRepository servicesJpaRepository) {
        this.vehicleJpaRepository = vehicleJpaRepository;
        this.visitJpaRepository = visitJpaRepository;
        this.servicesJpaRepository = servicesJpaRepository;
    }

    @Override
    public List<Vehicle> search(Optional<String> search) {
        return vehicleJpaRepository.findAll();
    }

    @Override
    public Vehicle getByVin(String vin) {
        return null;
    }

    @Override
    public Vehicle getById(int id) {
        return vehicleJpaRepository.findById(id);
    }

    @Override
    public void create(Vehicle vehicle, User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_VEHICLE_CREATION);
        }
        boolean duplicateExists = true;
        try {
            Vehicle vehicleToBeCreated = vehicleJpaRepository.findVehicleByVIN(vehicle.getVIN());
            if (vehicleToBeCreated == null) {
                throw new EntityNotFoundException("", "", "");
            }
            if (!ValidateLicensePlate.isValidLicensePlate(vehicleToBeCreated.getLicensePlate())){
                throw new InvalidLicensePlateException(vehicleToBeCreated.getLicensePlate());
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "vin", vehicle.getLicensePlate());
        }
        vehicleJpaRepository.save(vehicle);
    }

    @Override
    public void update(User user, Vehicle vehicle) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(VEHICLE_UPDATE_ERROR);
        }
        boolean duplicateExists = true;
        try {
            Vehicle vehicleToUpdate = vehicleJpaRepository.findVehicleByVIN(vehicle.getVIN());
            if (vehicleToUpdate.getId() == vehicle.getId()) {
                duplicateExists = false;
            }
        } catch (NullPointerException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "vin", vehicle.getLicensePlate());
        }
        vehicleJpaRepository.save(vehicle);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(VEHICLE_UPDATE_ERROR);
        }
        Vehicle vehicleToDelete = vehicleJpaRepository.findById(id);
        if (vehicleToDelete == null) {
            throw new EntityNotFoundException("", "", "");
        }
        List<Visit> visits = visitJpaRepository.findByVehicle(vehicleToDelete);
        for (Visit visit : visits) {
            visit.setVehicle(null);
            visitJpaRepository.save(visit);
        }
        vehicleJpaRepository.delete(vehicleToDelete);
    }

    @Override
    public void addUser(Vehicle vehicle, User user, User authorizedUser) {
        if (authorizedUser.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(VEHICLE_UPDATE_ERROR);
        }
        Vehicle vehicleToChange = vehicleJpaRepository.findById(vehicle.getId());
        if (vehicleToChange == null) {
            throw new EntityNotFoundException("", "", "");
        }
        if (vehicleToChange.getUser() != null){
            throw new UnsupportedOperationException(UNSUPPORTED_VEHICLE_ADD);
        }
        vehicle.setUser(user);
        vehicleJpaRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> getByOwner(int id) {
        return vehicleJpaRepository.filterVehicleByOwner(id);
    }

    @Override
    public List<Vehicle> getAll(){
        return vehicleJpaRepository.findAll();
    }

}
