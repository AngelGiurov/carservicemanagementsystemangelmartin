package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;

public class EditVehicleDto {

    public static final String INVALID_PLATE = "Valid bulgarian license plates contain between 7 and 8 characters";

    @JsonProperty(value = "licensePlate")
    private String licensePlate;


    @JsonProperty(value = "user")
    private int userId;

    public EditVehicleDto() {
    }


    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
