#Project Description

## Name
SmartGarage

## Description
SmartGarage is a web application that helps Employees of an auto repair shop
to manage their day-to-day job. Customers walk into the shop and explain what
the issue is with their vehicles and then an employee creates a new personal vehicle in the application.
When the shop visit is created the employee creates a new user, new
vehicle, and adds the services that will be performed on the car. The new customer receives
email, that contains information about his automatically generated link for account verification alongside one time only password.
Customers can send the specific service information to their emails.

## REST API
Swagger: http://localhost:8080/swagger-ui/#/

## Instructions how to set up and run the project locally:
1. Download the source code from https:https://gitlab.com/AngelGiurov/carservicemanagementsystemangelmartin
2. Start in your favourite integrated development environment.
3. Run on LocalHost.

## Database
<img src="https://res.cloudinary.com/dpe6hblnv/image/upload/v1651095485/database_cuclmk.png" alt="logo" width="1000px" style="margin-top: 20px;"/>
## Repo

```
cd existing_repo
git remote add oriin https://gitlab.com/AngelGiurov/carservicemanagementsystemangelmartin
git branch -M main
git push -uf origin main
```
