package com.smartgarage.models.mappers;

import com.smartgarage.models.Model;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.EditVehicleDto;
import com.smartgarage.models.dtos.VehicleDTO;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.VehicleService;
import com.smartgarage.services.contracts.MakeService;
import com.smartgarage.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapper {

    private final VehicleService vehicleService;
    private final UserService userService;
    private final ModelService modelService;

    public VehicleMapper(VehicleService vehicleService, UserService userService, ModelService makeService) {
        this.vehicleService = vehicleService;
        this.userService = userService;
        this.modelService = makeService;
    }

    public Vehicle createNewVehicle(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    public Vehicle addVehicleToUser(int vehicleId, User user) {
        Vehicle vehicle = vehicleService.getById(vehicleId);
        vehicle.setUser(user);
        return vehicle;
    }

    public Vehicle fromDto(EditVehicleDto editVehicleDto, int id) {
        Vehicle vehicle = vehicleService.getById(id);
        dtoToObject(editVehicleDto,vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDTO vehicleDTO, int id) {
        Vehicle vehicle = vehicleService.getById(id);
        dtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDTO, vehicle);
        return vehicle;
    }

    private void dtoToObject(EditVehicleDto editVehicleDto, Vehicle vehicle) {
        User user = userService.getById(editVehicleDto.getUserId());
        vehicle.setLicensePlate(editVehicleDto.getLicensePlate());
        vehicle.setUser(userService.getById(user.getId()));
    }

    private void dtoToObject(VehicleDTO vehicleDTO, Vehicle vehicle) {
        Model model = modelService.getById(vehicleDTO.getModelId());
        vehicle.setVIN(vehicleDTO.getVIN());
        vehicle.setLicensePlate(vehicleDTO.getLicensePlate());
        vehicle.setCreationYear(vehicleDTO.getCreationYear());
        vehicle.setModel(modelService.getById(model.getId()));
    }

}
