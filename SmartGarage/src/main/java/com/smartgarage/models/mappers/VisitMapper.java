package com.smartgarage.models.mappers;

import com.smartgarage.models.Service;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dtos.VisitDTO;
import com.smartgarage.services.contracts.ServicesService;
import com.smartgarage.services.contracts.VehicleService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class VisitMapper {
    private final VisitService visitService;
    private final VehicleService vehicleService;
    private final ServicesService servicesService;

    public VisitMapper(VisitService visitService, VehicleService vehicleService, ServicesService servicesService) {
        this.visitService = visitService;
        this.vehicleService = vehicleService;
        this.servicesService = servicesService;
    }

    public Visit fromDto(VisitDTO visitDTO) throws ParseException {
        Visit visit = new Visit();
        dtoToObject(visitDTO, visit);
        return visit;
    }

    public Visit fromDto(VisitDTO visitDTO, int id) throws ParseException {
        Visit visit = visitService.getById(id);
        dtoToObject(visitDTO, visit);
        return visit;
    }

    private void dtoToObject(VisitDTO visitDTO, Visit visit) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        String fromDate = visitDTO.getStartDate();
        Date date = (Date)formatter.parse(fromDate);
        visit.setStartDate(date);
        String toDate = visitDTO.getEndDate();
        Date date2 = (Date)formatter.parse(toDate);
        visit.setEndDate(date2);
        Set<Service> serviceSet = new HashSet<>();
        for (int i = 0; i < visitDTO.getServices().size(); i++){
            serviceSet.add(servicesService.getById(visitDTO.getServices().get(i)));
        }
        visit.setServices(serviceSet);
        visit.setVehicle(vehicleService.getById(visitDTO.getVehicleId()));
    }
}
