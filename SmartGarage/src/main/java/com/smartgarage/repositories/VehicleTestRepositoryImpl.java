package com.smartgarage.repositories;

import com.smartgarage.models.Vehicle;
import com.smartgarage.repositories.contracts.VehicleTestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

//@Repository
public class VehicleTestRepositoryImpl implements VehicleTestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleTestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(vehicle);
            session.getTransaction().commit();
        }


    }

    @Override
    public Vehicle getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Vehicle post = session.get(Vehicle.class, id);
            if (post == null) {
                throw new EntityNotFoundException();
            }
            return post;
        }
    }

    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle", Vehicle.class);
            return query.list();
        }
    }

}
