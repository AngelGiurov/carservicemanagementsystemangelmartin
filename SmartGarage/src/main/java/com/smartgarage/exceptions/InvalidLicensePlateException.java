package com.smartgarage.exceptions;

public class InvalidLicensePlateException extends RuntimeException{

    public InvalidLicensePlateException(String type) {
        super(String.format("%s license plate not valid.", type));
    }
}
