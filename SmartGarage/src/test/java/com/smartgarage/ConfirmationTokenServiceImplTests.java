package com.smartgarage;

import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.ConfirmationTokenJpaRepository;
import com.smartgarage.services.ConfirmationTokenServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ConfirmationTokenServiceImplTests {

    @Mock
    ConfirmationTokenJpaRepository repository;

    @InjectMocks
    ConfirmationTokenServiceImpl service;

    @Test
    void getToken_Should_CallRepository() {
        ConfirmationToken token = Helpers.createMockToken();
        User user = Helpers.createMockCustomer();

        service.create(user,2);

        Mockito.when(service
                .getToken(token.getToken()))
                .thenReturn(token);


        Mockito.verify(repository
                    ,Mockito.times(1))
                .findByToken(token.getToken());
    }

     /*

 @Test
    void getByID_Should_CallRepo_When_ValidInput() {
        User requester = Helper.createEmployee();
        Mockito.when(repository.getById(requester.getId())).thenReturn(requester);
        userService.getById(requester, requester.getId());
        Mockito.verify(repository, Mockito.times(1)).getById(requester.getId());
    }

      */

//    @Test
//    void create_Should_CallRepository() {
//        ResetPasswordTokens token = new ResetPasswordTokens();
//        service.create(token);
//        Mockito.verify(repository, Mockito.times(1)).create(token);
//    }
//
//    @Test
//    void update_Should_CallRepository() {
//        ResetPasswordTokens token = new ResetPasswordTokens();
//        service.update(token);
//        Mockito.verify(repository, Mockito.times(1)).update(token);
//    }

}
