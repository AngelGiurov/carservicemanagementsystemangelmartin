package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;

public class ModelDTO {

    public static final String MODEL_NAME_LENGTH_ERROR = "Model name length error";
    @JsonProperty(value = "name")
    @Size(min = 2, max = 32, message = MODEL_NAME_LENGTH_ERROR)
    private String name;

    @JsonProperty(value = "make")
    private int makeId;

    public ModelDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }
}
