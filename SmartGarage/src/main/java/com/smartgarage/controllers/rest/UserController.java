package com.smartgarage.controllers.rest;


import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.dtos.EditProfileDto;
import com.smartgarage.models.dtos.PasswordChangeDto;
import com.smartgarage.models.dtos.RegisterDto;
import com.smartgarage.models.dtos.UserDTO;
import com.smartgarage.models.dtos.out.UserOutDto;
import com.smartgarage.models.enums.UserSortOptions;
import com.smartgarage.models.mappers.UserMapper;
import com.smartgarage.repositories.UserRepositoryImpl;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.EditorKit;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")

public class UserController {
    public static final String PROFILE_CONFIRMED = "Profile confirmed";
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;


    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, UserRepositoryImpl userTestRepository) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserOutDto getUser(@PathVariable int id) {
        try {
            User user = userService.getById(id);
            return userMapper.fromObject(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserOutDto create(@Valid @RequestBody UserDTO userDto) {
        try {
            User user = userMapper.fromDto(userDto);
            userService.create(user);
            return userMapper.fromObject(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public UserOutDto update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                             @Valid @RequestBody EditProfileDto editProfileDto) {
        try {
            User authorizedUser = authenticationHelper.tryGetUser(headers);
            User userToBeUpdated = userMapper.fromDto(editProfileDto, id);
            userService.update(authorizedUser, userToBeUpdated);
            return userMapper.fromObject(userToBeUpdated);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authorizedUser = authenticationHelper.tryGetUser(headers);
            userService.delete(id, authorizedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/{username}")
    public User getByUsername(@PathVariable String username) {
        try {
            return userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{email}")
    public User getByEmail(@PathVariable String email) {
        try {
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/register")
    @ResponseStatus(value = HttpStatus.CREATED)
    public String registerUser(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody RegisterDto registerDto) {
        try {
            User authorizedUser = authenticationHelper.tryGetUser(headers);
            User userToBeUpdated = userMapper.fromDto(registerDto);
            return userService.registerUser(authorizedUser, userToBeUpdated);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/changePass")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void changePass(@RequestHeader HttpHeaders headers,
                           @Valid @RequestBody PasswordChangeDto passwordChangeDto) {
        try {
            User authorizedUser = authenticationHelper.tryGetUser(headers);
            userService.changePassword(authorizedUser, passwordChangeDto);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/confirm")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public String confirmUser(@RequestParam(name = "token") String token) {
        userService.confirmUser(token);
        return PROFILE_CONFIRMED;
    }

    @GetMapping("/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> username,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam(required = false) Optional<String> phoneNumber,
                             @RequestParam(required = false) Optional<String> model,
                             @RequestParam(required = false) Optional<String> make,
                             @RequestParam(required = false) Optional<String> visitStart,
                             @RequestParam(required = false) Optional<String> visitEnd,
                             @RequestParam(required = false) Optional<UserSortOptions> sortOptions) throws ParseException {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.filter(user, email, username, phoneNumber, make, model, visitStart, visitEnd, sortOptions);
    }

    //todo addvehicle to user

}

