package com.smartgarage;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Model;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.ModelJpaRepository;
import com.smartgarage.services.ModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {

    @Mock
    ModelJpaRepository modelJpaRepository;

    @InjectMocks
    ModelServiceImpl modelService;

    @Test
    void getById_Should_CallRepository(){
        Model model = Helpers.createMockModel();

        Mockito.when(modelJpaRepository.findById(model.getId())).thenReturn(model);

        modelService.getById(model.getId());
        Mockito.verify(modelJpaRepository,Mockito.times(1)).findById(model.getId());
    }

    @Test
    void findByName_Should_CallRepository() {
        Model model = Helpers.createMockModel();
        String test = "test";
        Mockito.when(modelJpaRepository.findModelByName(test)).thenReturn(model);

        modelService.findByName(test);

        Mockito.verify(modelJpaRepository, Mockito.times(1)).findModelByName(test);
    }

    @Test
    void createModel_Should_ThrowUnauthorizedOperationException_When_UserIsNotEmployee(){
        User user1 = Helpers.createMockCustomer();
        Model model = Helpers.createMockModel();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> modelService.createModel(user1,model));
    }
    @Test
    void createModel_Should_ThrowUnauthorizedOperationException_When_ModelExists(){
        User user1 = Helpers.createMockEmployee();
        Model model = Helpers.createMockModel();


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> modelService.createModel(user1,model));
    }
}
