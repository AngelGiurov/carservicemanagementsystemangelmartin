package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.smartgarage.project_constants.Constants.*;

public class EditProfileDto {
    @JsonProperty(value = "username")
    @Size(min = 2, max = 20, message = USERNAME_LENGTH_INVALID)
    private String username;

    @JsonProperty(value = "phone_number")
    @Size(min = 10,max = 10, message = USER_PHONE_LENGTH_ERROR)
    private String phoneNumber;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
