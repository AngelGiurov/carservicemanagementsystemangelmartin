package com.smartgarage.services.contracts;

import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.ForgottenPasswordResetDTO;
import com.smartgarage.models.dtos.PasswordChangeDto;
import com.smartgarage.models.enums.UserSortOptions;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    void create(User user);

    void update(User authorizedUser, User userToBeUpdated);

    void delete(int id, User authorizedUser);

    User getByEmail(String username);

    List<User> getAll();

    User getByUsername(String username);

    User getById(int id);

    String registerUser(User authorizedUser, User userToBeUpdated);

    void confirmUser(String token);

    void sendResetPasswordMail(String email);

    void resetUserPassword(ConfirmationToken confirmationToken,
                           ForgottenPasswordResetDTO forgottenPasswordResetDTO);

    void changePassword(User userToChangePassword, PasswordChangeDto passwordChangeDto);

    List<User> filter(User user, Optional<String> email, Optional<String> username, Optional<String> phoneNumber,
                      Optional<String> make, Optional<String> model, Optional<String> visitStart, Optional<String> visitEnd,
                      Optional<UserSortOptions> postSortOptions) throws ParseException;

    void addVehicle(User employee, User customer, Vehicle vehicle);
}
