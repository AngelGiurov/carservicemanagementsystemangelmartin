package com.smartgarage.services;

import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import com.smartgarage.repositories.contracts.ConfirmationTokenJpaRepository;
import com.smartgarage.services.contracts.ConfirmationTokenService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    public static final String TOKEN_HAS_ALREADY_BEEN_USED = "Token has already been used";
    public static final String EXPIRED_TOKEN = "Token has already expired";
    private final ConfirmationTokenJpaRepository confirmationTokenJpaRepository;

    public ConfirmationTokenServiceImpl(ConfirmationTokenJpaRepository confirmationTokenJpaRepository) {
        this.confirmationTokenJpaRepository = confirmationTokenJpaRepository;
    }

    @Override
    public ConfirmationToken getToken(String token) {
        return confirmationTokenJpaRepository.findByToken(token);
    }


    @Override
    public String create(User userToCreate, int hoursActive) {

        String token = UUID.randomUUID().toString();
        LocalDateTime expirationPeriod = LocalDateTime.now().plusHours(hoursActive);
        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                userToCreate,
                LocalDateTime.now(),
                expirationPeriod);
        confirmationTokenJpaRepository.save(confirmationToken);
        return token;
    }

    @Override
    public void setConfirmedAt(ConfirmationToken token) {
        confirmationTokenJpaRepository.save(token);
    }

    @Override
    public void checkIfTokenActive(ConfirmationToken tokenToBeChecked){

        if (tokenToBeChecked.getUsedOn() != null) {
            throw new IllegalStateException(TOKEN_HAS_ALREADY_BEEN_USED);
        }

        LocalDateTime expiresAt = tokenToBeChecked.getExpiryDate();
        if (expiresAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException(EXPIRED_TOKEN);
        }
    }
}
