package com.smartgarage.repositories.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.enums.UserSortOptions;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface UserRepository {
    User findById(int id);

    User findUserByUsername(String username);

    User findUserByEmail (String email);

    List<User> getAll();

    void create(User user);

    void update(User user);

    void delete(int id);

    List<User> filter(Optional<String> email, Optional<String> username, Optional<String> phoneNumber,
                      Optional<String>make, Optional<String>model, Optional<String>visitStart,
                      Optional<String>visitEnd, Optional<UserSortOptions> postSortOptions) throws ParseException;
}
