package com.smartgarage.controllers.rest;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.ServiceDTO;
import com.smartgarage.models.mappers.ServiceMapper;
import com.smartgarage.services.ServicesServiceImpl;
import com.smartgarage.services.contracts.ServicesService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final ServicesServiceImpl servicesService;
    private final AuthenticationHelper authenticationHelper;
    private final ServiceMapper serviceMapper;

    public ServiceController(ServicesServiceImpl servicesService, AuthenticationHelper authenticationHelper, ServiceMapper serviceMapper) {
        this.servicesService = servicesService;
        this.authenticationHelper = authenticationHelper;
        this.serviceMapper = serviceMapper;
    }

    @GetMapping
    public List<Service> getAll() {
        return servicesService.getAll();
    }

    @GetMapping("/{id}")
    public Service getService(@PathVariable int id) {
        try {
            return servicesService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders header, @Valid @RequestBody ServiceDTO serviceDTO) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Service service = serviceMapper.fromDto(serviceDTO);
            servicesService.create(service, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders header, @Valid @RequestBody ServiceDTO serviceDTO,
                       @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Service service = serviceMapper.fromDto(serviceDTO, id);
            servicesService.update(service, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            servicesService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filterByPrice")
    public List<Service> filterByPrice(@RequestHeader HttpHeaders headers,
                                       @RequestParam(name = "price", required = false) Optional<Double> price,
                                       @RequestParam(name = "name", required = false) Optional<String> name) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return servicesService.filterByPrice(user,price,name);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filterByUser")
    public List<Service> filterByUser(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return servicesService.getServicesByUser(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
