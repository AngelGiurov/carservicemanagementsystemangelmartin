package com.smartgarage.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOptions {
    NAME_ASC("Name, ascending", " order by user.username "),
    NAME_DESC("Name, descending", " order by user.username desc "),
    VISITDATE_ASC("Date, ascending"," order by visit.startDate asc "),
    VISITDATE_DSEC("Date, descending", " order by visit.endDate desc ");

    private final String preview;
    private final String query;

    private static final Map<String, UserSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    public static UserSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    UserSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
