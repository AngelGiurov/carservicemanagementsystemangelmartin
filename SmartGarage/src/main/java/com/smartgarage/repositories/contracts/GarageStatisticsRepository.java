package com.smartgarage.repositories.contracts;


public interface GarageStatisticsRepository {

    long getVehicles();

    long getCustomers();

    long getServices();

    long getVisits();
}
