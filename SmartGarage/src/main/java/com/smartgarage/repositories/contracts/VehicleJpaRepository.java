package com.smartgarage.repositories.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleJpaRepository extends JpaRepository<Vehicle, Integer> {

    Vehicle findVehicleByVIN(String VIN);

    Vehicle findById(int id);

    @Query(value = "Select e " +
            "from Vehicle e join e.user u where u.id = :id")
    List<Vehicle> filterVehicleByOwner(int id);

    Vehicle findVehicleByUser(User user);

}
