package com.smartgarage.services.contracts;

import com.smartgarage.models.Model;
import com.smartgarage.models.User;

import java.util.List;

public interface ModelService {
    Model getById(int id);

    Model findByName(String name);

    void createModel(User user, Model model);

    void updateModel(User user, Model model);

    List<Model> getAll();
}

