package com.smartgarage.models.dtos.out;

import com.smartgarage.models.Service;
import com.smartgarage.models.enums.Roles;

import java.util.Set;

public class UserOutDto {

    private String username;

    private Roles role;

    private String phone;

    private String email;

//    private String VIN;

//    public String getVIN() {
//        return VIN;
//    }
//
//    public void setVIN(String VIN) {
//        this.VIN = VIN;
//    }
//
//    public Set<Service> getServiceList() {
//        return serviceList;
//    }
//
//    public void setServiceList(Set<Service> serviceList) {
//        this.serviceList = serviceList;
//    }

//    private Set<Service> serviceList;

    public UserOutDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public Roles getRole() {
        return role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
