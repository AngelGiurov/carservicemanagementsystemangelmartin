package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Make;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MakeJpaRepository extends JpaRepository<Make,Integer> {

    Make findById(int id);

    Make findMakeByName(String name);
}
