package com.smartgarage.repositories.contracts;

import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfirmationTokenJpaRepository extends JpaRepository<ConfirmationToken, Integer> {

    ConfirmationToken findById(int id);

    ConfirmationToken findByToken(String token);

    List<ConfirmationToken> findConfirmationTokensByUser(User user);

}
