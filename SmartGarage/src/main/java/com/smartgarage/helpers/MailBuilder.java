package com.smartgarage.helpers;

import com.smartgarage.models.User;
import org.springframework.stereotype.Component;

@Component
public class MailBuilder {

    public static final String CONFIRM_TOKEN = "http://localhost:8080/api/users/confirm?token=";
    public static final String PASSWORD_BACK_UP = "http://localhost:8080/forgotten-password-reset?token=";
    public static final String ACTIVATION_MESSAGE = "Hello, %s, please follow the activation link: %s.%n" +
            "now you can login into your account.%n" +
            "Temporary password: %s";
    public static final String BACK_UP_MESSAGE = "Hello, %s," +
            "please follow the link to set your new password.%n:%s";

    public static String registrationMessage(User userToBeCreated, String token) {
        String activationLink = CONFIRM_TOKEN + token;
        return String.format(
                ACTIVATION_MESSAGE,
                userToBeCreated.getUsername(),
                activationLink,
                userToBeCreated.getPassword());
    }

    public static String passwordBackUpMessage(User userToReset, String token) {

        String passResetLink = PASSWORD_BACK_UP + token;
        return String.format(BACK_UP_MESSAGE,
                userToReset.getUsername(),
                passResetLink);
    }

}
