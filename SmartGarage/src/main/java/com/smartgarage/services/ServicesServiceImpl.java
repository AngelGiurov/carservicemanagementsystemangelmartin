package com.smartgarage.services;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.ServicesJpaRepository;
import com.smartgarage.repositories.contracts.UserRepository;
import com.smartgarage.repositories.contracts.VisitJpaRepository;
import com.smartgarage.services.contracts.ServicesService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ServicesServiceImpl implements ServicesService {

    public static final String SERVICE_CREATE_RESTRICTION = "Only employees can create services";
    public static final String SERVICE_UPDATE_ERROR = "Only admins can update service info";
    public static final String SERVICE_DELETE_ERROR = "Only employees can delete services";
    public static final String UNAUTHORIZED_OPERATION = "Unauthorized operation";
    private final ServicesJpaRepository servicesJpaRepository;
    private final UserRepository userRepository;
    private final VisitService visitService;
    private final VisitJpaRepository visitJpaRepository;

    @Autowired
    public ServicesServiceImpl(ServicesJpaRepository servicesJpaRepository, UserRepository userRepository, VisitService visitService, VisitJpaRepository visitJpaRepository) {
        this.servicesJpaRepository = servicesJpaRepository;
        this.userRepository = userRepository;
        this.visitService = visitService;
        this.visitJpaRepository = visitJpaRepository;
    }

    @Override
    public List<com.smartgarage.models.Service> getAll() {
        return servicesJpaRepository.findAll();
    }

    @Override
    public com.smartgarage.models.Service getById(int id) {
        return servicesJpaRepository.findById(id);
    }

    @Override
    public void create(com.smartgarage.models.Service service, User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(SERVICE_CREATE_RESTRICTION);
        }
        boolean duplicateExists = true;
        try {
            com.smartgarage.models.Service serviceToBeCreated = servicesJpaRepository.findByName(service.getName());
            if (serviceToBeCreated == null) {
                throw new EntityNotFoundException("", "", "");
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", service.getName());
        }
        servicesJpaRepository.save(service);
    }

    @Override
    public void update(com.smartgarage.models.Service service, User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(SERVICE_UPDATE_ERROR);
        }
        boolean duplicateExists = true;
        try {
            com.smartgarage.models.Service serviceToUpdate = servicesJpaRepository.findByName(service.getName());
            if (serviceToUpdate.getId() == service.getId()) {
                duplicateExists = false;
            }
        } catch (NullPointerException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Services", "service", service.getName());
        }
        servicesJpaRepository.save(service);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnsupportedOperationException(SERVICE_DELETE_ERROR);
        }
        com.smartgarage.models.Service serviceToDelete = servicesJpaRepository.findById(id);
        if (serviceToDelete == null) {
            throw new EntityNotFoundException("", "", "");
        }
        servicesJpaRepository.delete(serviceToDelete);
    }


    @Override
    public List<com.smartgarage.models.Service> filterByPrice(User user, Optional<Double> price, Optional<String> name){
//        if (user.getRole() != Roles.CUSTOMER || user.getRole()!=Roles.EMPLOYEE){
//            throw new UnsupportedOperationException(UNAUTHORIZED_OPERATION);
//        }
        return servicesJpaRepository.filterByPrice(price, name);
    }

    @Override
    public com.smartgarage.models.Service getByName(String name){
        return servicesJpaRepository.findByName(name);
    }

    @Override
    public List<com.smartgarage.models.Service> getServicesByUser(User user) {

        return servicesJpaRepository.getServicesByUserId(user.getId());
    }

}
