package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Vehicle;

import java.util.List;

public interface VehicleTestRepository {
    void create(Vehicle vehicle);

    Vehicle getById(int id);

    List<Vehicle> getAll();

}
