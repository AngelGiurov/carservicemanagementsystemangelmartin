package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.Visit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface VisitJpaRepository extends JpaRepository<Visit, Integer> {

    List<Visit> findAll();

    Visit findById(int id);

    Visit findVisitsByStartDateAfterAndEndDateBefore(LocalDateTime startDate, LocalDateTime endDate);

    @Query(value = "Select v, sum(s.price) as price from Visit v JOIN v.services s where v.id = :id")
    Visit getVisitInfo(int id);

    @Query(value = "Select v from Visit v join v.services s where v.id = :serviceId")
    List<Visit> byService(int serviceId);

    List<Visit> findByVehicleUser(User user);

    List<Visit> findByVehicle(Vehicle vehicle);

    Visit findByVehicleVIN(String VIN);

}
