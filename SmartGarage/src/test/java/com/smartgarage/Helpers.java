package com.smartgarage;

import com.smartgarage.models.*;
import com.smartgarage.models.enums.Roles;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    public static User createMockCustomer() {
        return createMockUser(Roles.CUSTOMER);
    }

    public static User createMockEmployee() {
        return createMockUser(Roles.EMPLOYEE);
    }

    private static User createMockUser(Roles userRole) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setPhone("0888855555");
        mockUser.setEnable(true);
        mockUser.setRole(userRole);
        return mockUser;
    }

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();
        mockVehicle.setId(1);
        mockVehicle.setCreationYear(1992);
        mockVehicle.setUser(createMockCustomer());
        mockVehicle.setModel(createMockModel());
        mockVehicle.setLicensePlate("ВТ1234ВТ");
        mockVehicle.setVIN("mockVin");
        return mockVehicle;
    }

    public static Model createMockModel() {
        var mockModel= new Model();
        mockModel.setModel_id(1);
        mockModel.setName("TestName");
        mockModel.setId(1);
        mockModel.setMake(createMockMake());
        return mockModel;
    }

    public static Make createMockMake() {
        var mockMake = new Make();
        mockMake.setId(1);
        mockMake.setName("TestName");
        return mockMake;
    }

    public static Service createMockService() {
        var mockService = new Service();
        mockService.setId(1);
        mockService.setName("TestName");
        mockService.setPrice(1.0);
        return mockService;
    }

    public static Visit createMockVisit() throws ParseException {
        var mockVisit = new Visit();
        mockVisit.setId(createMockCustomer().getId());
        mockVisit.setStartDate(DateFormat.getDateInstance().parse("16-06-199"));
        Set<Service> services = new HashSet<>();
        mockVisit.setServices(services);
        mockVisit.setVehicle(createMockVehicle());
        return mockVisit;
    }

   public static ConfirmationToken createMockToken(){
        var mockToken = new ConfirmationToken();
        mockToken.setToken("TESTTOKEN123");
        mockToken.setCreateOn(LocalDateTime.now());
        mockToken.setId(1L);
        mockToken.setExpiryDate(LocalDateTime.now().plusHours(1));
        mockToken.setUsedOn(LocalDateTime.now().plusMinutes(30));
        return mockToken;
   }

    public static List<Vehicle> getListOfVehiclesFromOwner(int id) {
        User user1 = createMockCustomer();
        Vehicle vehicle1 = createMockVehicle();
        Vehicle vehicle2 = createMockVehicle();
        Vehicle vehicle3 = createMockVehicle();
        vehicle1.setUser(user1);
        vehicle2.setUser(user1);
        vehicle3.setUser(user1);
        return List.of(vehicle1, vehicle2, vehicle3);
    }
}
