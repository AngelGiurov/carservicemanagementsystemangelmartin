package com.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.smartgarage.project_constants.Constants.USER_PASSWORD_PATTERN_INVALID;

public class PasswordChangeDto {

    @NotNull
    private String oldPassword;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
            message = USER_PASSWORD_PATTERN_INVALID)
    private String newPassword;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$",
            message = USER_PASSWORD_PATTERN_INVALID)
    private String repeatNewPassword;

    public PasswordChangeDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatNewPassword() {
        return repeatNewPassword;
    }

    public void setRepeatNewPassword(String repeatNewPassword) {
        this.repeatNewPassword = repeatNewPassword;
    }
}
