package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.services.contracts.GarageStatisticsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final GarageStatisticsService garageStatisticsService;

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    private final AuthenticationHelper authenticationHelper;


    public HomeMvcController(GarageStatisticsService garageStatisticsService, AuthenticationHelper authenticationHelper) {
        this.garageStatisticsService = garageStatisticsService;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public String showHomePage (Model model, HttpSession session){
        User currentUser;
    try {
        currentUser = authenticationHelper.tryGetUser(session);
        model.addAttribute("currentUser", currentUser);
    }catch (UnauthorizedOperationException e){
         model.addAttribute("currentUser",null);
    }
        return "index";
    }

    @ModelAttribute("vehicles")
    public long getNumberOfVehicles() {
        return garageStatisticsService.getVehicles();
    }
    @ModelAttribute("customers")
    public long getNumberOfCustomers() {
        return garageStatisticsService.getCustomers();
    }
    @ModelAttribute("visits")
    public long getNumberOfVisits() {
        return garageStatisticsService.getVisits();
    }
    @ModelAttribute("services")
    public long getNumberOfServices() {
        return garageStatisticsService.getServices();
    }
}
