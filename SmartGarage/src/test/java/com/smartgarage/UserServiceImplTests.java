package com.smartgarage;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.UserSortOptions;
import com.smartgarage.repositories.contracts.UserRepository;
import com.smartgarage.repositories.contracts.VehicleJpaRepository;
import com.smartgarage.services.ConfirmationTokenServiceImpl;
import com.smartgarage.services.UserServiceImpl;
import com.smartgarage.services.contracts.EmailSender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.smartgarage.models.enums.UserSortOptions.NAME_ASC;


@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository repository;

    @Mock
    VehicleJpaRepository vehicleJpaRepository;

    @Mock
    EmailSender emailSender;

    @InjectMocks
    ConfirmationTokenServiceImpl confirmationTokenService;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void getAll_Should_CallRepository() {
        userService.getAll();

        Mockito.verify(repository,Mockito.times(1)).getAll();
    }

    @Test
    void getByUsername_Should_CallRepository() {
        User user = Helpers.createMockCustomer();

        Mockito.when(repository.findUserByUsername(user.getUsername())).thenReturn(user);

        userService.getByUsername(user.getUsername());

        Mockito.verify(repository, Mockito.times(1)).findUserByUsername(user.getUsername());
    }

    @Test
    void getByUsername_Should_ThrowEntityNotFoundException_When_UserDoesNotExist() {
        User user = Helpers.createMockCustomer();

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername(user.getUsername()));
    }

    @Test
    void getByEmail_Should_CallRepository() {
        User user = Helpers.createMockCustomer();

        Mockito.when(repository.findUserByEmail(user.getEmail())).thenReturn(user);

        userService.getByEmail(user.getEmail());

        Mockito.verify(repository, Mockito.times(1)).findUserByEmail(user.getEmail());
    }

    @Test
    void getByEmail_Should_ThrowEntityNotFoundException_When_UserDoesNotExist() {
        User user = Helpers.createMockCustomer();

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByEmail(user.getEmail()));
    }

    @Test
    void getByID_Should_CallRepository(){
        User user = Helpers.createMockCustomer();

        Mockito.when(repository.findById(user.getId())).thenReturn(user);

        userService.getById(user.getId());

        Mockito.verify(repository, Mockito.times(1)).findById(user.getId());
    }

    @Test
    void registerUser_Should_ThrowUnsupportedOperationException_When_UserIsNotEmployee(){
        User authorizedUser = Helpers.createMockCustomer();
        User userToBeRegistered = Helpers.createMockCustomer();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> userService.registerUser(authorizedUser,userToBeRegistered));
    }

    @Test
    void registerUser_Should_ThrowDuplicateEntityException_When_UserExists(){
        User authorizedUser = Helpers.createMockEmployee();
        User userToBeRegistered = Helpers.createMockCustomer();

        Mockito.doThrow(DuplicateEntityException.class)
                .when(repository).create(userToBeRegistered);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.registerUser(authorizedUser,userToBeRegistered));
    }

    @Test
    void registerUser_Should_CallRepository(){
        User authorizedUser = Helpers.createMockEmployee();
        User userToBeRegistered = Helpers.createMockCustomer();

        Assertions.assertDoesNotThrow(() ->
                userService.registerUser
                        (authorizedUser,userToBeRegistered));

        Mockito.verify
                (confirmationTokenService,Mockito.times(1)
                ).create(userToBeRegistered,2);

        Mockito.verify(repository,Mockito.times(1))
                .create(userToBeRegistered);

        //TODO fix

    }

    @Test
    void create_Should_Throw_When_DuplicateUsername() {
        User userToBeCreated = Helpers.createMockCustomer();

        Mockito.doThrow(DuplicateEntityException.class)
                .when(repository).create(userToBeCreated);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(userToBeCreated));
    }


    @Test
    void create_Should_CallRepository() {
        User userToBeCreated = Helpers.createMockCustomer();

        Assertions.assertDoesNotThrow(() -> userService.create(userToBeCreated));

        Mockito.verify(repository,Mockito.times(1)).create(userToBeCreated);
    }

    @Test
    void create_Should_ThrowDuplicateEntityException_When_UserExists(){
        User user = Helpers.createMockCustomer();

        Mockito.doThrow(DuplicateEntityException.class)
                .when(repository).create(user);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(user));
    }

    @Test
    void update_Should_ThrowUnsupportedOperationException_When_UserIsNotOwner(){
        User user1 = Helpers.createMockCustomer();
        User user2 = Helpers.createMockCustomer();

        user2.setId(3);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(user1,user2));
    }

    @Test
    void update_Should_ThrowDuplicateEntityException_When_UserExist(){

        User existingUser = Helpers.createMockCustomer();
        User authorizedUser = Helpers.createMockEmployee();
        User userToBeUpdated = Helpers.createMockCustomer();


        Mockito.doThrow(DuplicateEntityException.class)
                .when(repository).update(userToBeUpdated);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(authorizedUser,userToBeUpdated));

        //TODO fix
    }


    @Test
    void update_Should_CallRepository(){
        User authorizedUser = Helpers.createMockCustomer();
        User userToBeCreated = Helpers.createMockCustomer();
        User existingUser = Helpers.createMockCustomer();

        Mockito.when(repository.findUserByUsername(userToBeCreated.getUsername()))
                .thenReturn(existingUser);

        Assertions.assertDoesNotThrow(() ->
                userService.update(authorizedUser,userToBeCreated));

        Mockito.verify(repository,Mockito.times(1)).update(userToBeCreated);
    }

    @Test
    void delete_Should_CallRepository(){
        User userToBeDeleted = Helpers.createMockCustomer();

        userToBeDeleted.setUsername("delete");

        User authorizedUser = Helpers.createMockEmployee();

        Mockito.when(repository.findById(userToBeDeleted.getId()))
                .thenReturn(userToBeDeleted);

        Assertions.assertDoesNotThrow(() -> userService.delete(userToBeDeleted.getId(),authorizedUser));

        Mockito.verify(repository,Mockito.times(1))
                .delete(userToBeDeleted.getId());
    }

    @Test
    void delete_Should_ThrowUnauthorizedOperationException_When_InitiatorIsNotEmployee(){
        User userToBeDeleted = Helpers.createMockCustomer();

        User authorizedUser = Helpers.createMockCustomer();
        authorizedUser.setUsername("testing12343");

        Mockito.when(repository.findById(userToBeDeleted.getId()))
                .thenReturn(userToBeDeleted);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(userToBeDeleted.getId(),authorizedUser));

    }
}
