package com.smartgarage.helpers;

import java.util.Arrays;

public class ValidateLicensePlate {

    public static boolean isValidLicensePlate(String licensePlate) {
        String[] plates = new String[]
                {"ВТ", "ВН", "ВР", "ЕВ",
                        "ТХ", "КН", "ОВ",
                        "РА", "РК", "ЕН", "РВ",
                        "РР", "СС", "СН", "СМ",
                        "СО", "СА", "СВ", "СТ",
                        "Е", "А", "В", "К",
                        "Т", "Х", "Н", "У",
                        "М", "Р", "С",
                };
        if (licensePlate.length() != 8) {
            return false;
        }
        String singlePlate = licensePlate.substring(0, 1);
        String doublePlate = licensePlate.substring(0, 2);
        return Arrays.asList(plates).contains(singlePlate) || Arrays.asList(plates).contains(doublePlate);
    }
}
