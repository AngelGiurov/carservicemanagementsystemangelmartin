package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;

import static com.smartgarage.project_constants.Constants.USER_PASSWORD_PATTERN_INVALID;

public class RegisterDto extends LoginDto {
    public static final String EMAIL_LENGTH_RESTRICTION = "Email length restriction";

    @JsonProperty(value = "email")
    @Email
    @Size(min = 5, max = 40, message = EMAIL_LENGTH_RESTRICTION)
    private String email;

    @JsonProperty(value = "phone")
    private String phoneNumber;

    public String getPhone() {
        return phoneNumber;
    }

    public void setPhone(String phone) {
        this.phoneNumber = phone;
    }

}
