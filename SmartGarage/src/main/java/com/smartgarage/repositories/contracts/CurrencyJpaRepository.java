package com.smartgarage.repositories.contracts;

import com.smartgarage.models.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyJpaRepository extends JpaRepository<Currency, Integer> {
    Currency findByName(String name);
}
