package com.smartgarage.models.dtos;

public class BrandDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
