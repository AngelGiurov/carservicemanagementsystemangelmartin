package com.smartgarage.services.contracts;

import com.smartgarage.models.Make;
import com.smartgarage.models.User;

import java.util.List;

public interface MakeService {
    Make getById(int id);

    void createMake(User user, String makeName);

    void updateMake(User user, Make make);

    List<Make> getAll();
}
