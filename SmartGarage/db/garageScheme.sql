create or replace table currency
(
    id   int         not null
        primary key,
    name varchar(15) null
);

create or replace table databasechangeloglock
(
    ID          int          not null
        primary key,
    LOCKED      bit          not null,
    LOCKGRANTED datetime     null,
    LOCKEDBY    varchar(255) null
);

create or replace table make
(
    make_id int auto_increment
        primary key,
    name    varchar(20) null
);

create or replace table model
(
    model_id int auto_increment
        primary key,
    name     varchar(20) not null,
    make_id  int         not null,
    constraint model_make_make_id_fk
        foreign key (make_id) references make (make_id)
);

create or replace table service_types
(
    type_id int auto_increment
        primary key,
    name    varchar(20) not null,
    price   double      not null
);

create or replace table user
(
    user_id      int auto_increment
        primary key,
    username     varchar(50)          null,
    password     varchar(50)          not null,
    email        varchar(50)          not null,
    phone_number varchar(10)          not null,
    role         varchar(15)          not null,
    enabled      tinyint(1) default 0 not null,
    constraint user_email_uindex
        unique (email),
    constraint user_phone_number_uindex
        unique (phone_number),
    constraint user_username_uindex
        unique (username)
);

create or replace table confirmation_token
(
    id          int auto_increment
        primary key,
    user_id     int                                     null,
    token       varchar(150)                            not null,
    expiry_date timestamp default current_timestamp()   not null on update current_timestamp(),
    used_on     timestamp                               null,
    created_on  timestamp default '0000-00-00 00:00:00' not null,
    constraint confirmation_token_user_user_id_fk
        foreign key (user_id) references user (user_id)
);

create or replace table vehicle
(
    vehicle_id    int auto_increment
        primary key,
    VIN           varchar(17) not null,
    model_id      int         not null,
    license_plate varchar(10) not null,
    creation_year int         not null,
    user_id       int         null,
    constraint vehicle_VIN_uindex
        unique (VIN),
    constraint vehicle_license_plate_uindex
        unique (license_plate),
    constraint vehicle_model_model_id_fk
        foreign key (model_id) references model (model_id),
    constraint vehicle_user_user_id_fk
        foreign key (user_id) references user (user_id)
);

create or replace table visit
(
    visit_id   int auto_increment
        primary key,
    end_date   datetime null,
    start_date datetime null,
    vehicle_id int      null,
    constraint visit_vehicle_vehicle_id_fk
        foreign key (vehicle_id) references vehicle (vehicle_id)
);

create or replace table services
(
    service_type_id int not null,
    visit_id        int null,
    constraint services_service_types_type_id_fk
        foreign key (service_type_id) references service_types (type_id),
    constraint services_visit_visit_id_fk
        foreign key (visit_id) references visit (visit_id)
);

