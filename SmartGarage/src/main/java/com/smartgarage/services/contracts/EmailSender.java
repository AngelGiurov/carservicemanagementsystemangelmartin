package com.smartgarage.services.contracts;

public interface EmailSender {
    void send(String to, String subject, String text);
}
