package com.smartgarage.models.dtos;

public class FilterServicesDTO {
    private String name;

    private Double price;

    public FilterServicesDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
