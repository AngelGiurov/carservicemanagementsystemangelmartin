package com.smartgarage.controllers.rest;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Model;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.ModelDTO;
import com.smartgarage.models.dtos.VehicleDTO;
import com.smartgarage.models.mappers.ModelMapper;
import com.smartgarage.models.mappers.VehicleMapper;
import com.smartgarage.services.contracts.MakeService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final ModelService modelService;
    private final MakeService makeService;
    private final UserService userService;

    @Autowired
    public VehicleController(VehicleService vehicleService, VehicleMapper vehicleMapper, AuthenticationHelper authenticationHelper, ModelMapper modelMapper, ModelService modelService, MakeService makeService, UserService userService) {
        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
        this.makeService = makeService;
        this.userService = userService;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestParam(required = false) Optional<String> search) {
        return vehicleService.search(search);
    }


    @GetMapping("/{id}")
    public Vehicle getVehicle(@PathVariable int id) {
        try {
            return vehicleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders header, @Valid @RequestBody VehicleDTO vehicleDto) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Vehicle vehicle = vehicleMapper.createNewVehicle(vehicleDto);
            vehicleService.create(vehicle, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders header, @Valid @RequestBody VehicleDTO vehicleDto, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Vehicle vehicle = vehicleMapper.fromDto(vehicleDto, id);
            vehicleService.update(user, vehicle);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Vehicle vehicle = vehicleService.getById(id);
            vehicleService.delete(vehicle.getId(), user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/models")
    public void createModel(@RequestHeader HttpHeaders header, @Valid @RequestBody ModelDTO modelDTO){
        try {
            User user = authenticationHelper.tryGetUser(header);
            Model model = modelMapper.fromDto(modelDTO);
            modelService.createModel(user, model);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/makes")
    public void createMake(@RequestHeader HttpHeaders header, @Valid @RequestBody String make){
        try {
            User user = authenticationHelper.tryGetUser(header);
            makeService.createMake(user, make);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filterByOwner")
    public List<Vehicle> getByOwner(@RequestHeader HttpHeaders headers,
                              @RequestParam(name = "id",required = false) Optional<Integer> id,
                              @RequestParam(name = "ownerId",required = false) Optional<Integer> ownerId){
        if (id.isPresent()){
            List<Vehicle> list = new ArrayList<>();
            list.add(vehicleService.getById(id.get()));
            return list;
        } else if (ownerId.isPresent()){
            userService.getById(ownerId.get());
            return vehicleService.getByOwner(ownerId.get());
        } else {
            return vehicleService.getAll();
        }
    }




}
