package com.smartgarage.services.contracts;

import com.smartgarage.models.Service;
import com.smartgarage.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ServicesService {
    List<Service> getAll();

    Service getById(int id);

    void create(Service service, User user);

    void update(Service service, User user);

    void delete(int id, User user);

    List<Service> filterByPrice(User user, Optional<Double> price, Optional<String> name);

    Service getByName(String name);

    List<Service> getServicesByUser(User user);

}
