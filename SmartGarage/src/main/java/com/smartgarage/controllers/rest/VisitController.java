package com.smartgarage.controllers.rest;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dtos.ServiceDTO;
import com.smartgarage.models.dtos.VisitDTO;
import com.smartgarage.models.mappers.ServiceMapper;
import com.smartgarage.models.mappers.VisitMapper;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final ServiceMapper serviceMapper;
    private final AuthenticationHelper authenticationHelper;


    public VisitController(VisitService visitService, VisitMapper visitMapper, ServiceMapper serviceMapper, AuthenticationHelper authenticationHelper) {
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.serviceMapper = serviceMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Visit> getAll() {
        return visitService.getAll();
    }

    @PostMapping()
    public void create(@RequestHeader HttpHeaders header, @Valid @RequestBody VisitDTO visitDTO) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Visit visit = visitMapper.fromDto(visitDTO);
            visitService.create(visit, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders header, @Valid @RequestBody VisitDTO visitDTO,
                       @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Visit visit = visitMapper.fromDto(visitDTO, id);
            visitService.update(visit, user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            visitService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/service")
    public Visit addService(@RequestHeader HttpHeaders header, @PathVariable int id,
                             @Valid @RequestBody ServiceDTO serviceDTO) {
        try {
            User user = authenticationHelper.tryGetUser(header);
            Visit visit = visitService.getById(id);
            Service service = serviceMapper.fromDto(serviceDTO);
            return visitService.addService(visit, service, user);
        } catch (com.smartgarage.exceptions.EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

//    @PostMapping("/{id}/sendReport")
//    public void getReport(@PathVariable int id, @RequestHeader HttpHeaders headers) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            visitService.sendReportEmail(id, visitReportDto.getCurrencyId(), user);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @PostMapping("/currencies")
    public void currencies() throws IOException {
        visitService.currencyConverter();
    }


}
