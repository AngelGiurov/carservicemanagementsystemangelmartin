package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.ConfirmationToken;
import com.smartgarage.models.User;
import com.smartgarage.models.dtos.ForgottenPasswordResetDTO;
import com.smartgarage.models.dtos.PassRestartRequestDto;
import com.smartgarage.models.dtos.PasswordChangeDto;
import com.smartgarage.services.contracts.ConfirmationTokenService;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
@RequestMapping
public class PasswordResetMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final ConfirmationTokenService confirmationTokenService;
    private final UserService userService;

    @Autowired
    public PasswordResetMvcController(AuthenticationHelper authenticationHelper,
                                      ConfirmationTokenService confirmationTokenService,
                                      UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.confirmationTokenService = confirmationTokenService;
        this.userService = userService;
    }

    @GetMapping("/request-pass-restart")
    public String showPassRestartRequestPage(Model model) {
        model.addAttribute("passRestartRequestDto", new PassRestartRequestDto());
        return "forgetPassword";
    }

    @PostMapping("/request-pass-restart")
    public String handlePassRestartRequest(@Valid @ModelAttribute("email")
                                                   String email,
                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "forgetPassword";
        }
        userService.sendResetPasswordMail(email);
        return "redirect:/";
    }

    @GetMapping("/forgotten-password-reset")
    public String showPassRestartPage(@RequestParam(name = "token") String token,
                                      Model model, HttpSession session) {

        session.setAttribute("passRestartToken", token);
        model.addAttribute("forgottenPasswordReset", new ForgottenPasswordResetDTO());

        ConfirmationToken tokenToDeactivate = confirmationTokenService.getToken(token);
        if (tokenToDeactivate.isUsed()) {
            return "not-found";
        }
        if (tokenToDeactivate.getExpiryDate().isBefore(LocalDateTime.now())) {
            return "not-found";
        }
        confirmationTokenService.setConfirmedAt(tokenToDeactivate);
        return "password-reset";
    }

    @PostMapping("/forgotten-password-reset")
    public String handlePassRestart(@Valid @ModelAttribute("forgottenPasswordReset")
                                            ForgottenPasswordResetDTO forgottenPasswordResetDTO,
                                    BindingResult bindingResult,
                                    HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "password-reset";
        }
        String token = (String) session.getAttribute("passRestartToken");

        if (!forgottenPasswordResetDTO.getPassword().equals(forgottenPasswordResetDTO.getRepeatPassword())) {
            bindingResult.rejectValue("repeatPassword", "password_error", "Passwords don't match!");
        }

        try {
            ConfirmationToken confirmationToken = confirmationTokenService.getToken(token);
            userService.resetUserPassword(confirmationToken, forgottenPasswordResetDTO);
            session.removeAttribute("passRestartToken");
            return "redirect:/";

        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("password", "password_error", e.getMessage());
            return "password-reset";
        } catch (IllegalStateException e) {
            bindingResult.rejectValue("repeatPassword", "password_error", e.getMessage());
            return "password-reset";
        }



    }


}


