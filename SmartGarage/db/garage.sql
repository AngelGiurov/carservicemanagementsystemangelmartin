-- --------------------------------------------------------
-- Хост:
-- Версия на сървъра:            10.6.5-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дъмп структура за таблица garage.confirmation_token
CREATE TABLE IF NOT EXISTS `confirmation_token` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
    `token` varchar(150) NOT NULL,
    `expiry_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    `used_on` timestamp NULL DEFAULT NULL,
    `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`id`),
    KEY `confirmation_token_user_user_id_fk` (`user_id`),
    CONSTRAINT `confirmation_token_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.confirmation_token: ~21 rows (приблизително)
/*!40000 ALTER TABLE `confirmation_token` DISABLE KEYS */;
INSERT INTO `confirmation_token` (`id`, `user_id`, `token`, `expiry_date`, `used_on`, `created_on`) VALUES
(10, 20, 'aaaf1214-4c91-4ffb-ac8c-0e4d31796969', '2022-04-07 03:30:55', '2022-04-04 15:33:04', '2022-04-04 15:30:55'),
(11, 21, '3ad7a267-8c85-4ec7-bb9f-e1c9eeccad35', '2022-04-07 03:37:10', NULL, '2022-04-04 15:37:10'),
(12, 20, '7c20ed82-29b6-43f0-a7e7-5cfdcc604e99', '2022-04-08 04:56:48', NULL, '2022-04-05 16:56:48'),
(13, 20, 'c6bf5067-dbae-4cc1-8bb6-a9d6a7df9cac', '2022-04-08 06:58:41', NULL, '2022-04-05 18:58:41'),
(14, 20, '86e20db1-82fb-4e8b-8191-695db493f62d', '2022-04-08 06:58:49', NULL, '2022-04-05 18:58:49'),
(15, 20, '07bdfbad-485a-4c7e-948e-9f3256852919', '2022-04-08 06:59:32', NULL, '2022-04-05 18:59:35'),
(16, 20, '881af1b4-fbf9-4692-9752-ebaf335a273d', '2022-04-08 07:01:55', NULL, '2022-04-05 19:01:55'),
(17, 20, '9116eeab-33cf-4a5f-9dee-f3ea84bd51b2', '2022-04-08 07:07:04', NULL, '2022-04-05 19:07:04'),
(18, 20, '4d9ad964-ac4b-4381-90db-7232ccba99de', '2022-04-08 07:10:15', NULL, '2022-04-05 19:10:15'),
(19, 20, 'bdfa6345-380b-42ea-a25b-c39ecc00057a', '2022-04-08 07:18:47', NULL, '2022-04-05 19:18:47'),
(20, 20, '3202ca6b-e480-4989-bf27-c4f0b80d5119', '2022-04-08 09:32:30', NULL, '2022-04-05 21:32:30'),
(21, 20, '2e6ddbd6-f45e-4aea-a024-01643ede8511', '2022-04-08 09:47:45', NULL, '2022-04-05 21:47:45'),
(22, 20, '3c130a5b-8b80-48ef-936a-56e3a48830e7', '2022-04-08 12:11:24', NULL, '2022-04-06 00:11:24'),
(23, 20, 'c5b9f584-9b13-4994-b050-f4318b1fc206', '2022-04-08 12:22:56', '2022-04-06 00:35:59', '2022-04-06 00:22:56'),
(24, 23, '33bd989f-af1b-4a2d-88de-70956ba0d3eb', '2022-06-05 12:30:39', '2022-04-06 12:31:34', '2022-04-06 12:30:39'),
(25, 23, 'dc1e3f3c-de07-43f3-a4dd-7bdca8cff482', '2022-04-09 00:32:54', NULL, '2022-04-06 12:32:54'),
(30, 32, '1f2c8656-4371-4bcd-9fd8-4964e3ae85e2', '2022-06-13 09:29:02', '2022-04-14 09:29:10', '2022-04-14 09:29:02'),
(31, 33, '552024ec-71e6-48c5-b5c2-46432df3fc45', '2022-06-13 11:32:27', NULL, '2022-04-14 11:32:27'),
(32, 34, '2d047339-d288-48b0-be55-10b4b7de257b', '2022-06-13 17:00:01', '2022-04-14 17:00:16', '2022-04-14 17:00:01'),
(33, 36, '68ef8509-6ea8-47e4-84a2-2dde290fbf49', '2022-06-13 17:54:21', NULL, '2022-04-14 17:54:21'),
(34, 38, '55499c6c-9b5e-4ba1-8da2-e99f6d84ad28', '2022-06-13 17:58:34', NULL, '2022-04-14 17:58:34'),
(35, 38, '34cfaba7-3a86-4999-8eef-cd85e31b09da', '2022-04-21 21:51:23', NULL, '2022-04-19 09:51:23'),
(36, 38, 'e7e67624-f8bc-4113-94a7-b0e3574db688', '2022-04-21 21:51:24', NULL, '2022-04-19 09:51:24');
/*!40000 ALTER TABLE `confirmation_token` ENABLE KEYS */;

-- Дъмп структура за таблица garage.currency
CREATE TABLE IF NOT EXISTS `currency` (
    `id` int(11) NOT NULL,
    `name` varchar(15) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.currency: ~0 rows (приблизително)
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;

-- Дъмп структура за таблица garage.databasechangeloglock
CREATE TABLE IF NOT EXISTS `databasechangeloglock` (
    `ID` int(11) NOT NULL,
    `LOCKED` bit(1) NOT NULL,
    `LOCKGRANTED` datetime DEFAULT NULL,
    `LOCKEDBY` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`ID`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.databasechangeloglock: ~0 rows (приблизително)
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;

-- Дъмп структура за таблица garage.make
CREATE TABLE IF NOT EXISTS `make` (
    `make_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`make_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.make: ~4 rows (приблизително)
/*!40000 ALTER TABLE `make` DISABLE KEYS */;
INSERT INTO `make` (`make_id`, `name`) VALUES
(4, 'Mercedes'),
(7, 'BMW'),
(10, 'Volkswagen');
/*!40000 ALTER TABLE `make` ENABLE KEYS */;

-- Дъмп структура за таблица garage.model
CREATE TABLE IF NOT EXISTS `model` (
    `model_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(20) NOT NULL,
    `make_id` int(11) NOT NULL,
    PRIMARY KEY (`model_id`),
    KEY `model_make_make_id_fk` (`make_id`),
    CONSTRAINT `model_make_make_id_fk` FOREIGN KEY (`make_id`) REFERENCES `make` (`make_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.model: ~10 rows (приблизително)
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` (`model_id`, `name`, `make_id`) VALUES
(3, 'W222', 4),
(4, 'W211', 4),
(5, 'G-class', 4),
(7, 'S-class', 4),
(8, 'C-class', 4),
(9, 'GOLF', 10),
(10, 'Passat', 10),
(11, 'E92', 7),
(12, 'E46', 7),
(13, 'F10', 7);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;

-- Дъмп структура за таблица garage.services
CREATE TABLE IF NOT EXISTS `services` (
    `service_type_id` int(11) NOT NULL,
    `visit_id` int(11) DEFAULT NULL,
    KEY `services_service_types_type_id_fk` (`service_type_id`),
    KEY `services_visit_visit_id_fk` (`visit_id`),
    CONSTRAINT `services_service_types_type_id_fk` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`type_id`),
    CONSTRAINT `services_visit_visit_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visit` (`visit_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.services: ~26 rows (приблизително)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`service_type_id`, `visit_id`) VALUES
(2, 1),
(3, 1),
(2, 2),
(2, 2),
(3, 20),
(7, 20),
(8, 21),
(9, 21),
(11, 22),
(12, 22),
(14, 23),
(13, 23),
(11, 23),
(7, 24),
(8, 24),
(8, 25),
(9, 25),
(3, 28),
(7, 28),
(8, 28),
(13, 29),
(10, 29),
(14, 29),
(9, 30),
(7, 30),
(3, 30);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- Дъмп структура за таблица garage.service_types
CREATE TABLE IF NOT EXISTS `service_types` (
    `type_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(20) NOT NULL,
    `price` double NOT NULL,
    PRIMARY KEY (`type_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.service_types: ~10 rows (приблизително)
/*!40000 ALTER TABLE `service_types` DISABLE KEYS */;
INSERT INTO `service_types` (`type_id`, `name`, `price`) VALUES
(2, 'oil change', 22),
(3, 'tyre change', 21),
(7, 'new tyres', 26),
(8, 'exhaust system ', 50),
(9, 'Car A/C repair', 40),
(10, 'Inspection', 15),
(11, 'Engine Cooling', 60),
(12, 'Maintenance', 40),
(13, 'battery change', 25),
(14, 'Diagnostics', 15);
/*!40000 ALTER TABLE `service_types` ENABLE KEYS */;

-- Дъмп структура за таблица garage.user
CREATE TABLE IF NOT EXISTS `user` (
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(50) DEFAULT NULL,
    `password` varchar(50) NOT NULL,
    `email` varchar(50) NOT NULL,
    `phone_number` varchar(10) NOT NULL,
    `role` varchar(15) NOT NULL,
    `enabled` tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `user_email_uindex` (`email`),
    UNIQUE KEY `user_phone_number_uindex` (`phone_number`),
    UNIQUE KEY `user_username_uindex` (`username`)
    ) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.user: ~11 rows (приблизително)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `phone_number`, `role`, `enabled`) VALUES
(1, 'bat_marta', 'Sm@rtg1rag', 'martin@abv.bg12', '0888888888', 'EMPLOYEE', 1),
(20, 'Martow', '1@34567Aa', 'martiparti1656@gmail.com', '0818124123', 'CUSTOMER', 1),
(21, 'Ivo', '12@Aa56789', 'dimievivo@gmail.com', '0888888883', 'CUSTOMER', 1),
(23, 'Viktor', '12@Aa56789', 'todorov.martin1699@gmail.com', '0884888883', 'CUSTOMER', 1),
(24, 'Ivow', '12@Aa56789', 'aidasd@gmail.com', '0888912341', 'CUSTOMER', 1),
(32, 'Vladi', '1@34567Aaa', 'martiparti111366@gmail.com', '0123456789', 'CUSTOMER', 1),
(33, 'Toshko', '1@34567Aa', 'martiparti1616@gmail.com', '0887646606', 'CUSTOMER', 1),
(34, 'martincho1', '1@34567Aaaa', 'martiparti1662gmail.com', '3123124144', 'CUSTOMER', 1),
(36, 'Pavka', '1@34567Aa', 'martiparti1166@gmail.com', '0881646606', 'CUSTOMER', 1),
(38, 'Vanko', '1@34567Aa', 'martiparti166@gmail.com', '0883246606', 'CUSTOMER', 1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Дъмп структура за таблица garage.vehicle
CREATE TABLE IF NOT EXISTS `vehicle` (
    `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
    `VIN` varchar(17) NOT NULL,
    `model_id` int(11) NOT NULL,
    `license_plate` varchar(10) NOT NULL,
    `creation_year` int(11) NOT NULL,
    `user_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`vehicle_id`),
    UNIQUE KEY `vehicle_VIN_uindex` (`VIN`),
    UNIQUE KEY `vehicle_license_plate_uindex` (`license_plate`),
    KEY `vehicle_user_user_id_fk` (`user_id`),
    KEY `vehicle_model_model_id_fk` (`model_id`),
    CONSTRAINT `vehicle_model_model_id_fk` FOREIGN KEY (`model_id`) REFERENCES `model` (`model_id`),
    CONSTRAINT `vehicle_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.vehicle: ~11 rows (приблизително)
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` (`vehicle_id`, `VIN`, `model_id`, `license_plate`, `creation_year`, `user_id`) VALUES
(19, '21341521122312211', 3, 'CT2131AK', 2012, 38),
(20, '32141251213121222', 12, 'CA2123BT', 2014, 20),
(21, '12312312341221122', 10, 'CBXXXXXX', 2022, 23),
(24, '12312412312312311', 5, 'CB1234AM', 2005, 32),
(25, '21312341231312111', 7, 'CB3123AK', 2004, 33),
(26, '12312312312322222', 8, 'B3333AX', 2001, 34),
(27, '82739128318231987', 5, 'H9812AX', 1995, 36),
(28, '38721327183123111', 4, 'A9999EH', 2022, 38),
(29, '21321333321122312', 11, 'Y3571AP', 2004, 21),
(30, '21312344124444444', 9, 'P3213AK', 2001, 24),
(31, '88321827101827301', 13, 'CT9999AK', 2004, 1);
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;

-- Дъмп структура за таблица garage.visit
CREATE TABLE IF NOT EXISTS `visit` (
    `visit_id` int(11) NOT NULL AUTO_INCREMENT,
    `end_date` datetime DEFAULT NULL,
    `start_date` datetime DEFAULT NULL,
    `vehicle_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`visit_id`),
    KEY `visit_vehicle_vehicle_id_fk` (`vehicle_id`),
    CONSTRAINT `visit_vehicle_vehicle_id_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица garage.visit: ~15 rows (приблизително)
/*!40000 ALTER TABLE `visit` DISABLE KEYS */;
INSERT INTO `visit` (`visit_id`, `end_date`, `start_date`, `vehicle_id`) VALUES
(1, '2022-04-02 00:00:00', '2022-04-01 00:00:00', 20),
(2, '2022-04-03 00:00:00', '2022-03-08 00:00:00', 20),
(10, '2022-04-09 17:07:51', '2022-05-09 17:08:06', 19),
(18, NULL, '2022-04-22 09:36:05', 21),
(19, NULL, '2022-07-20 09:36:10', 24),
(20, NULL, '2022-05-21 09:36:13', 25),
(21, NULL, '2022-04-21 09:36:25', 26),
(22, NULL, '2022-08-22 09:36:33', 27),
(23, NULL, '2021-05-09 09:36:52', 28),
(24, NULL, '2022-05-03 09:37:24', 29),
(25, NULL, '2022-06-22 09:37:31', 30),
(26, NULL, '2022-06-16 10:20:27', 31),
(28, '2022-06-18 00:00:00', '2022-06-18 00:00:00', 25),
(29, '2022-06-19 00:00:00', '2022-06-19 00:00:00', 25),
(30, '2022-06-22 00:00:00', '2022-06-22 00:00:00', 31);
/*!40000 ALTER TABLE `visit` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
