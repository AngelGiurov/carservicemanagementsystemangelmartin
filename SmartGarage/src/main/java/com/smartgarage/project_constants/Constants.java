package com.smartgarage.project_constants;

public class Constants {

    //Authentication constants
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";

    // User Constants
    public static final String USER_ID_CANNOT_BE_NULL = "Id cannot be null";
    public static final String USER_ID_MUST_BE_POSITIVE = "Id must be a positive integer";
    public static final String USERNAME_INVALID = "Username cannot be empty.";
    public static final String USERNAME_LENGTH_INVALID = "Username should be between 2 and 20 symbols.";
    public static final String USER_PASSWORD_INVALID = "Password cannot be empty.";
    public static final String USER_PASSWORD_PATTERN_INVALID = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)";
    public static final String USER_NOT_OWNER = "Only user owners can update their personal info";
    public static final String USER_PHONE_EXISTS = "An user already owns this phone number.";
    public static final String USER_PHONE_LENGTH_ERROR = "Phone number must be 10 digits";
    public static final String EMAIL_CANNOT_BE_EMPTY = "Email cannot be empty";
    public static final String USER_PASSWORD_CANNOT_BE_EMPTY = "Password cannot be empty";
    public static final String EMAIL_LENGTH_RESTRICTION = "Email length restriction";

    // Employee
    public static final String ONLY_EMPLOYEES_CAN_DELETE_USER_PROFILE =
            "Only employees can delete this user";


    // Vehicle Constants
    public static final String VEHICLE_ID_CANNOT_BE_NULL = "Id cannot be null";
    public static final String VEHICLE_ID_MUST_BE_POSITIVE = "Id must be a positive integer";
    public static final String LICENSE_PLATE_ERROR = "The license plate must be a valid Bulgarian license plate";
    public static final String VIN_LENGTH_ERROR =
            "The vehicle identification number must be 17 characters long string.";
    public static final String YEAR_OF_CREATION_ERROR =
            "The year of creation must be a positive whole number larger than 1886.";
    public static final String MODEL_LENGTH_ERROR = "The model name must be between 2 and 32 symbols.";
    public static final String BRAND_LENGTH_ERROR = "The brand name must be between 2 and 32 symbols.";
}
