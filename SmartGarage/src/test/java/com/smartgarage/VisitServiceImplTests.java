package com.smartgarage;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.repositories.contracts.ServicesJpaRepository;
import com.smartgarage.repositories.contracts.VisitJpaRepository;
import com.smartgarage.services.VisitServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitJpaRepository visitJpaRepository;

    @Mock
     ServicesJpaRepository servicesJpaRepository;

    @InjectMocks
    VisitServiceImpl visitService;

    @Test
    void getById_Should_CallRepository() throws ParseException {
        Visit visit = Helpers.createMockVisit();

        Mockito.when(visitJpaRepository.findById(visit.getId())).thenReturn(visit);

        visitService.getById(visit.getId());

        Mockito.verify(visitJpaRepository, Mockito.times(1)).findById(visit.getId());
    }

    @Test
    void getAll_Should_CallRepository() {
        visitService.getAll();

        Mockito.verify(visitJpaRepository,Mockito.times(1)).findAll();
    }

    @Test
    void create_Should_Throw_When_UserIsNotEmployee() throws ParseException {
        User userToBeCreated = Helpers.createMockCustomer();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.create(visit,userToBeCreated));
    }

    @Test
    void create_Should_CallRepository() throws ParseException {
        User userToBeCreated = Helpers.createMockEmployee();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertDoesNotThrow(() -> visitService.create(visit,userToBeCreated));

        Mockito.verify(visitJpaRepository,Mockito.times(1)).save(visit);
    }

    @Test
    void update_Should_Throw_When_UserIsNotEmployee() throws ParseException {
        User userToBeCreated = Helpers.createMockCustomer();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.update(visit,userToBeCreated));
    }

    @Test
    void update_Should_CallRepository() throws ParseException {
        User userToBeCreated = Helpers.createMockEmployee();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertDoesNotThrow(() -> visitService.update(visit,userToBeCreated));

        Mockito.verify(visitJpaRepository,Mockito.times(1)).save(visit);
    }

    @Test
    void delete_Should_Throw_When_UserIsNotEmployee() throws ParseException {
        User userToBeCreated = Helpers.createMockCustomer();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> visitService.delete(1,userToBeCreated));
    }

    @Test
    void delete_Should_CallRepository() throws ParseException {
        User userToBeCreated = Helpers.createMockEmployee();
        Visit visit = Helpers.createMockVisit();

        Assertions.assertDoesNotThrow(() -> visitService.update(visit,userToBeCreated));

        Mockito.verify(visitJpaRepository,Mockito.times(1)).save(visit);
    }

    @Test
    void delete_Should_ThrowEntityNotFoundException_When_VisitNotFound() throws ParseException {
        User user = Helpers.createMockEmployee();
        Visit visit = Helpers.createMockVisit();

        Mockito.doThrow(EntityNotFoundException.class)
                .when(visitJpaRepository.findById(5));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> visitService.delete(4,user));

    }
}
