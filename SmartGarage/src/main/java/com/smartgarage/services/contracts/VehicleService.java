package com.smartgarage.services.contracts;

import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {
    void create(Vehicle vehicle, User user);

    List<Vehicle> search(Optional<String> search);

    Vehicle getByVin(String vin);

    Vehicle getById(int id);

    void update(User user, Vehicle vehicle);

    void delete(int id, User user);

    List<Vehicle> getByOwner(int id);

    List<Vehicle> getAll();

    void addUser(Vehicle vehicle, User user, User authorizedUser);
}
