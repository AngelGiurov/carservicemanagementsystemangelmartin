package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.AuthenticationFailureException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Service;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dtos.*;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.enums.UserSortOptions;
import com.smartgarage.models.mappers.UserMapper;
import com.smartgarage.models.mappers.VehicleMapper;
import com.smartgarage.models.mappers.VisitMapper;
import com.smartgarage.services.contracts.ServicesService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VehicleService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    public static final String CURRENT_USER = "currentUser";
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleMapper vehicleMapper;
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final UserMapper userMapper;
    private final VehicleService vehicleService;
    private final ServicesService servicesService;

    @ModelAttribute("userSortOptions")
    public UserSortOptions[] populateSortOptions() {
        return UserSortOptions.values();
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute(CURRENT_USER) != null;
    }

    @ModelAttribute("allVehicles")
    public List<Vehicle> populateVehicles(HttpSession session) {
        return vehicleService.getAll();
    }

    @ModelAttribute("vehicleServices")
    public List<Service> populateVehicleServices(HttpSession session) {
        return servicesService.getAll();
    }

    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, VehicleMapper vehicleMapper, VisitService visitService, VisitMapper visitMapper, UserMapper userMapper, VehicleService vehicleService, ServicesService servicesService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleMapper = vehicleMapper;
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.userMapper = userMapper;
        this.vehicleService = vehicleService;
        this.servicesService = servicesService;
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        model.addAttribute("currentUser",currentUser);
        model.addAttribute("users", userService.getAll());
        model.addAttribute("filterUsersDto", new FilterUsersDTO());
        model.addAttribute("editProfileDto",new EditProfileDto());
        model.addAttribute("visitDto", new VisitDTO());
        return "users";
    }

    @GetMapping("/{id}")
    public String userPage(@PathVariable int id, Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            User user = userService.getById(id);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("user", user);
            model.addAttribute("vehicles", vehicleService.getByOwner(user.getId()));
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("filterUsersDto", new FilterUsersDTO());
        model.addAttribute("editProfileDto", new EditProfileDto());
        model.addAttribute("vehicle", new Vehicle());
        model.addAttribute("visitDto", new VisitDTO());
        return "user";
    }

    @PostMapping("/filter")
    public String filterPosts(@ModelAttribute("filterUsersDto") FilterUsersDTO filterUsersDTO,
                              Model model, HttpSession session) throws ParseException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        var filtered = userService.filter(user,
                Optional.ofNullable(filterUsersDTO.getEmail().isBlank()  ? null : filterUsersDTO.getEmail()),
                Optional.ofNullable(filterUsersDTO.getUsername().isBlank() ? null : filterUsersDTO.getUsername()),
                Optional.ofNullable(filterUsersDTO.getPhoneNumber().isBlank() ? null : filterUsersDTO.getPhoneNumber()),
                Optional.ofNullable(filterUsersDTO.getMake().isBlank() ? null : filterUsersDTO.getMake()),
                Optional.ofNullable(filterUsersDTO.getModel().isBlank() ? null : filterUsersDTO.getModel()),
                Optional.ofNullable(filterUsersDTO.getStartDate().isBlank() ? null : filterUsersDTO.getStartDate()),
                Optional.ofNullable(filterUsersDTO.getEndDate().isBlank() ? null : filterUsersDTO.getEndDate()),
                Optional.ofNullable(filterUsersDTO.getSort() == null ? null : UserSortOptions.valueOfPreview(filterUsersDTO.getSort()))
        );
        model.addAttribute("editProfileDto", new EditProfileDto());
        model.addAttribute("users", filtered);
        return "users";
    }

    @PostMapping("/{id}/vehicle")
    public String addVehicle(@Valid @ModelAttribute("vehicle") Vehicle vehicle, @PathVariable int id,
                         BindingResult errors, Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return "my-profile";
        }
        User employee;
        try {
             employee = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            User customer = userService.getById(id);
            Vehicle vehicleToAdd = vehicleService.getById(vehicle.getId());
            userService.addVehicle(employee, customer, vehicleToAdd);
            return "redirect:/users/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e){
            model.addAttribute("error", e.getMessage());
            return "unauthorized-access";
        }
    }

    @PostMapping("/{id}/update")
    public String userInfoChange(@PathVariable int id, @Valid @ModelAttribute("editProfileDto")
            EditProfileDto editProfileDto,
                                 Model model,
                                 BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/user";
        }
        User authorizedUser;
        try {
            authorizedUser = userService.getById(1);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            model.addAttribute("editProfileDto", new EditProfileDto());
            model.addAttribute("filterServicesDto", new FilterServicesDTO());
            User userToBeUpdated = userMapper.fromDto(editProfileDto, id);
            userService.update(authorizedUser, userToBeUpdated);
            return "redirect:/users/" + id;
        } catch (AuthenticationFailureException e) {

            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "redirect:/users";

        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        }
    }

    @PostMapping("/{id}/delete")
    public String userDelete(@PathVariable int id,
                             HttpSession session) {
        User authorizedUser;
        try {
            authorizedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            userService.delete(id, authorizedUser);
        } catch (AuthenticationFailureException e) {
            return "redirect:/users";

        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        }
        return "redirect:/users";
    }

    @PostMapping("/{id}/visit")
    public String registerVisit(@PathVariable int id, @Valid @ModelAttribute("visitDto")
                                              VisitDTO visitDto,
                                      BindingResult bindingResult,
                                      HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/users";
        }
        User authorizedUser;
        try {
            authorizedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            Visit visitToCreate = visitMapper.fromDto(visitDto);
            visitService.create(visitToCreate, authorizedUser);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "redirect:/users/" + id;
    }


}
