package com.smartgarage.repositories;

import com.smartgarage.repositories.contracts.GarageStatisticsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class GarageStatisticsRepositoryImpl implements GarageStatisticsRepository {
    private final SessionFactory sessionFactory;

    public GarageStatisticsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public long getVehicles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(id) from Vehicle",Long.class);
            return query.getSingleResult();
        }
    }

    @Override
    public long getCustomers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(id) from User ",Long.class);
            return query.getSingleResult();
        }
    }

    @Override
    public long getServices() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(id) from Service ",Long.class);
            return query.getSingleResult();
        }
    }

    @Override
    public long getVisits() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(id) from Visit ",Long.class);
            return query.getSingleResult();
        }
    }
}
