package com.smartgarage.services;

import com.smartgarage.repositories.contracts.GarageStatisticsRepository;
import com.smartgarage.services.contracts.GarageStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GarageStatisticsServiceImpl implements GarageStatisticsService {

    private final GarageStatisticsRepository garageStatisticsRepository;

    @Autowired
    public GarageStatisticsServiceImpl(GarageStatisticsRepository garageStatisticsRepository) {
        this.garageStatisticsRepository = garageStatisticsRepository;
    }

    @Override
    public long getVehicles() {
        return garageStatisticsRepository.getVehicles();
    }

    @Override
    public long getCustomers() {
        return garageStatisticsRepository.getCustomers();
    }

    @Override
    public long getServices() {
        return garageStatisticsRepository.getServices();
    }

    @Override
    public long getVisits() {
        return garageStatisticsRepository.getVisits();
    }
}
