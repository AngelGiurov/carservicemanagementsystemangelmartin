package com.smartgarage.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "confirmation_token")
public class ConfirmationToken {

    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "token")
    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column(name = "created_on")
    private LocalDateTime createOn;

    @Column(name = "expiry_date")
    private LocalDateTime expiryDate;

    @Column(name = "used_on")
    private LocalDateTime usedOn;

    public ConfirmationToken(String token,
                             User user,
                             LocalDateTime createOn,
                             LocalDateTime expiryDate) {
        this.token = token;
        this.user = user;
        this.createOn = createOn;
        this.expiryDate = expiryDate;
    }

    public ConfirmationToken() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public LocalDateTime getUsedOn() {
        return usedOn;
    }

    public LocalDateTime getCreateOn() {
        return createOn;
    }

    public void setCreateOn(LocalDateTime createOn) {
        this.createOn = createOn;
    }

    public void setUsedOn(LocalDateTime usedOn) {
        this.usedOn = usedOn;
    }

    public boolean isUsed(){
        boolean isUsed;

        String confirmedAtStr = String.valueOf(getUsedOn());
        isUsed = !confirmedAtStr.equals("null");
        return isUsed;
    }

}
