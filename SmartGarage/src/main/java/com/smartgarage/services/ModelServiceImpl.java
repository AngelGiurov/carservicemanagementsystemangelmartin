package com.smartgarage.services;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Model;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.repositories.contracts.ModelJpaRepository;
import com.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    public static final String UNAUTHORIZED_VEHICLE_CREATION = "Only employees can create vehicles";
    public static final String UNAUTHORIZED_MODEL_UPDATE = "Only employees can create vehicles";
    public static final String MODEL = "Model";
    public static final String NAME = "name";
    private final ModelJpaRepository modelJpaRepository;

    @Autowired
    public ModelServiceImpl(ModelJpaRepository modelJpaRepository) {
        this.modelJpaRepository = modelJpaRepository;
    }

    @Override
    public Model getById(int id) {
        return modelJpaRepository.findById(id);
    }

    @Override
    public Model findByName(String name){
        return modelJpaRepository.findModelByName(name);
    }

    @Override
    public void createModel(User user, Model model) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_VEHICLE_CREATION);
        }
        boolean duplicateExists = true;
        try {
            Model modelToBeCreated = modelJpaRepository.findModelByName(model.getName());
            if (modelToBeCreated == null) {
                throw new EntityNotFoundException("", "", "");
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(MODEL, NAME, model.getName());
        }
        modelJpaRepository.save(model);
    }

    @Override
    public void updateModel(User user, Model model) {
        if (user.getRole() != Roles.EMPLOYEE) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_MODEL_UPDATE);
        }
        modelJpaRepository.save(model);
    }

    @Override
    public List<Model> getAll() {
        return modelJpaRepository.findAll();
    }
}
