package com.smartgarage.models.mappers;

import com.smartgarage.models.Model;
import com.smartgarage.models.dtos.ModelDTO;
import com.smartgarage.services.contracts.MakeService;
import com.smartgarage.services.contracts.ModelService;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

    private final ModelService modelService;
    private final MakeService makeService;

    public ModelMapper(ModelService modelService, MakeService makeService) {
        this.modelService = modelService;
        this.makeService = makeService;
    }

    public Model fromDto(ModelDTO modelDTO) {
        Model model = new Model();
        dtoToObject(modelDTO, model);
        return model;
    }

    public Model fromDto(ModelDTO modelDTO, int id) {
        Model model = modelService.getById(id);
        dtoToObject(modelDTO, model);
        return model;
    }

    private void dtoToObject(ModelDTO modelDTO, Model model) {
        model.setName(modelDTO.getName());
        model.setMake(makeService.getById(modelDTO.getMakeId()));
    }

}
