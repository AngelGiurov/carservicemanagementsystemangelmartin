package com.smartgarage.controllers;

import com.smartgarage.exceptions.AuthenticationFailureException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.services.UserServiceImpl;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import static com.smartgarage.project_constants.Constants.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

import static org.apache.tomcat.websocket.Constants.AUTHORIZATION_HEADER_NAME;

@Component
public class AuthenticationHelper {
    public static final String INACTIVE_ACCOUNT = "This account is inactive.";
    public static final String WRONG_PASSWORD = "Wrong password.";
    public static final String WRONG_EMAIL = "Wrong email";
    public static final String AUTHENTICATION_REQUIRED = "The request source requires authentication";
    public static final String INVALID_USERNAME = "Invalid username";
    public static final String NO_LOGGED_IN_USER = "No logged in user";
    public static final String USER_EMAIL = "userEmail";
    private final UserService service;

    @Autowired
    public AuthenticationHelper(UserServiceImpl service) {
        this.service = service;
    }
    public User tryGetUser (HttpHeaders headers){
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)){
            throw new ResponseStatusException
                    (HttpStatus.UNAUTHORIZED, AUTHENTICATION_REQUIRED);
        }
        try {
            String name = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return service.getByUsername(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME);
        }
    }

    public void verifyAuthentication(String email, String password) {
        try {
            User user = service.getByEmail(email);
            if (!user.isEnable()){
                throw new AuthenticationFailureException(INACTIVE_ACCOUNT);
            }
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(WRONG_PASSWORD);
            }
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(WRONG_EMAIL);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserEmail = (String) session.getAttribute(USER_EMAIL);
        if (currentUserEmail == null) {
            throw new UnauthorizedOperationException(NO_LOGGED_IN_USER);
        }
        try {
            return service.getByEmail(currentUserEmail);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(NO_LOGGED_IN_USER);
        }
    }
}
