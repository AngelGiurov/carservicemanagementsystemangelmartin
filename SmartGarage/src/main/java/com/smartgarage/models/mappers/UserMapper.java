package com.smartgarage.models.mappers;

import com.smartgarage.models.User;
import com.smartgarage.models.dtos.EditProfileDto;
import com.smartgarage.models.dtos.RegisterDto;
import com.smartgarage.models.dtos.UserDTO;
import com.smartgarage.models.dtos.out.UserOutDto;
import com.smartgarage.models.enums.Roles;
import com.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class UserMapper {

    private final UserService userService;


    @Autowired
    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User fromDto(UserDTO userDTO) {
        User user = new User();
        dtoToObject(userDTO, user);
        return user;
    }
    public User fromDto(UserDTO userDTO, int id) {
        User user = userService.getById(id);
        dtoToObject(userDTO, user);
        return user;
    }

    public User fromDto(EditProfileDto editProfileDto, int id) {
        User user = userService.getById(id);
        dtoToObject(editProfileDto, user);
        return user;
    }

    public UserOutDto fromObject(User user){
        UserOutDto userOut = new UserOutDto();
        ObjectToDto(user, userOut);
        return userOut;
    }

    private void ObjectToDto(User user,UserOutDto userOutDto){
        userOutDto.setEmail(user.getEmail());
        userOutDto.setUsername(user.getUsername());
        userOutDto.setPhone(user.getPhone());
        userOutDto.setRole(user.getRole());
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        String password = new Random().ints(10, 33, 122).collect(StringBuilder::new,
                        StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        password += "a)1A";
        user.setPassword(password);
        user.setUsername(registerDto.getEmail().substring(0,registerDto.getEmail().indexOf("@")));
        user.setPhone(registerDto.getPhone());
        user.setEmail(registerDto.getEmail());
        user.setRole(Roles.CUSTOMER);
        return user;
    }

    private void dtoToObject(UserDTO userDTO, User user) {
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setPhone(userDTO.getPhoneNumber());
    }

    private void dtoToObject(EditProfileDto editProfileDto, User user) {
        user.setUsername(editProfileDto.getUsername());
        user.setPhone(editProfileDto.getPhoneNumber());
    }
}
