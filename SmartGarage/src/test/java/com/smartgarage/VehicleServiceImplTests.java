package com.smartgarage;

import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.InvalidLicensePlateException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.repositories.contracts.VehicleJpaRepository;
import com.smartgarage.services.VehicleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleJpaRepository vehicleJpaRepository;

    @InjectMocks
    VehicleServiceImpl vehicleService;

    @Test
    void getById_Should_CallRepository() {

        Vehicle vehicle = Helpers.createMockVehicle();

        Mockito.when(vehicleJpaRepository
                .findById(vehicle.getId()))
                .thenReturn(vehicle);

        vehicleService.getById(vehicle.getId());

        Mockito.verify
                (vehicleJpaRepository,Mockito.times(1))
                .findById(vehicle.getId());
    }
    //Test search

    @Test
    void create_Should_Throw_When_UserIsNotEmployee() {
        User user = Helpers.createMockCustomer();
        Vehicle vehicle = Helpers.createMockVehicle();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> vehicleService.create(vehicle,user));
    }

    @Test
    void create_Should_CallRepository() {
        User user = Helpers.createMockEmployee();
        Vehicle vehicle = Helpers.createMockVehicle();

        Assertions.assertDoesNotThrow(() -> vehicleService.create(vehicle,user));

        Mockito.verify(vehicleJpaRepository,Mockito.times(1)).save(vehicle);
    }

    @Test
    void create_Should_ThrowDuplicateEntityException_When_UserExists() {
        User user = Helpers.createMockEmployee();
        Vehicle vehicle = Helpers.createMockVehicle();

        Mockito.doThrow(DuplicateEntityException.class)
                .when(vehicleJpaRepository).save(vehicle);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.create(vehicle,user));
    }

    @Test
    void create_Should_ThrowInvalidLicensePlateException_When_InvalidPlate() {
        User user = Helpers.createMockEmployee();
        Vehicle vehicle = Helpers.createMockVehicle();

        vehicle.setLicensePlate("564TESTTSTTSTSSTSTST");

        Mockito.doThrow(InvalidLicensePlateException.class)
                .when(vehicleJpaRepository).save(vehicle);

        Assertions.assertThrows(InvalidLicensePlateException.class,
                () -> vehicleService.create(vehicle,user));
    }

    @Test
    void update_Should_ThrowUnsupportedOperationException_When_UserIsNotEmployee(){
        User user = Helpers.createMockCustomer();
        Vehicle vehicle = Helpers.createMockVehicle();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> vehicleService.update(user,vehicle));
    }

    @Test
    void update_Should_CallRepository(){
        User authorizedUser = Helpers.createMockEmployee();
        Vehicle vehicle = Helpers.createMockVehicle();


        Mockito.when(vehicleJpaRepository.save(vehicle))
                .thenReturn(vehicle);

        Assertions.assertDoesNotThrow(() -> vehicleService.update(authorizedUser,vehicle));

        Mockito.verify(vehicleJpaRepository,Mockito.times(1)).save(vehicle);
    }

    @Test
    void update_Should_ThrowDuplicateEntityException_When_VehicleExists(){
        User user = Helpers.createMockCustomer();
        Vehicle vehicle = Helpers.createMockVehicle();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> vehicleService.update(user,vehicle));

        //TODO FIX
    }

    @Test
    void delete_Should_CallRepository(){
        User authorizedUser = Helpers.createMockEmployee();
        Vehicle vehicle = Helpers.createMockVehicle();

        Mockito.when(vehicleJpaRepository
                .findById(vehicle.getId()))
                .thenReturn(vehicle);

        Assertions.assertDoesNotThrow(() -> vehicleService.delete(vehicle.getId(),authorizedUser));

        Mockito.verify(vehicleJpaRepository,Mockito.times(1))
                .delete(vehicle);
    }

    @Test
    void delete_Should_ThrowUnauthorizedOperationException_When_InitiatorIsNotEmployee(){
        User user = Helpers.createMockCustomer();
        Vehicle vehicle = Helpers.createMockVehicle();

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> vehicleService.delete(vehicle.getId(),user));

    }

    @Test
    void getByOwner_Should_ReturnOwnerVehicles(){
        User user = Helpers.createMockCustomer();

        vehicleService.getByOwner(user.getId());
        Mockito.verify
                (vehicleJpaRepository.filterVehicleByOwner
                        (user.getId()));
    }

    @Test
    void getAll_Should_CallRepository() {
        vehicleService.getAll();

        Mockito.verify
                (vehicleJpaRepository,Mockito.times(1))
                .findAll();
    }

}
