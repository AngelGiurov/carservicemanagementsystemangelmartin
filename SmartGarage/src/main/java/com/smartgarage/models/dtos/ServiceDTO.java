package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ServiceDTO {

    public static final String SERVICE_NAME_LENGTH_ERROR = "Service name length error";
    public static final String INVALID_PRICE = "Invalid price";
    @JsonProperty(value = "type")
    @Size(min = 2, max = 32, message = SERVICE_NAME_LENGTH_ERROR)
    private String type;

    @JsonProperty(value = "price")
    @Positive(message = INVALID_PRICE)
    private double price;

    public ServiceDTO() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
