package com.smartgarage.repositories;

import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.models.User;
import com.smartgarage.models.enums.UserSortOptions;
import com.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;
    private final EntityManager entityManager;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, EntityManager entityManager) {
        this.sessionFactory = sessionFactory;
        this.entityManager = entityManager;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToBeDeleted = findById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToBeDeleted);
            session.getTransaction().commit();
        }
    }

    @Override
    public User findById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User findUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Email", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User findUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public List<User> filter(Optional<String> email, Optional<String> username, Optional<String> phoneNumber,
                             Optional<String> make, Optional<String> model, Optional<String> visitStart, Optional<String> visitEnd,
                             Optional<UserSortOptions>sortOptions) throws ParseException {
        var baseQuery = new StringBuilder();
        var filters = new ArrayList<String>();
        var params = new HashMap<String, Object>();
        try (Session session = sessionFactory.openSession()) {
            String queryStringWithDates =
                    "select user from Visit as visit" +
                            " join visit.vehicle as vehicle" +
                            " join vehicle.user as user" +
                            " where user.role = 'CUSTOMER'";

            String queryStringWithModelMake =
                    " select user from Vehicle as vehicle" +
                            " join vehicle.user as user" +
                            " where user.role = 'CUSTOMER' ";

            String queryStandard =
                    " select user from User as user" +
                            " where user.role = 'CUSTOMER' ";

            if (visitStart.isPresent() || visitEnd.isPresent() || sortOptions.isPresent()) {
                baseQuery.append(queryStringWithDates);
            } else if (model.isPresent() || make.isPresent()) {
                baseQuery.append(queryStringWithModelMake);
            } else {
                baseQuery.append(queryStandard);
            }

            email.ifPresent(value -> {
                filters.add(" user.email like :email");
                params.put("email", "%" + value + "%");
            });

            username.ifPresent(value -> {
                filters.add(" user.username like :username");
                params.put("username", "%" + value + "%");
            });

            phoneNumber.ifPresent(value -> {
                filters.add(" user.phoneNumber like :phoneNumber");
                params.put("phoneNumber", "%" + value + "%");
            });

            model.ifPresent(value -> {
                filters.add(" vehicle.model.name like :model");
                params.put("model", "%" + value + "%");
            });

            make.ifPresent(value -> {
                filters.add(" vehicle.model.make.name like :make");
                params.put("make", "%" + value + "%");
            });
            visitStart.ifPresent(value -> filters.add(" visit.startDate > :visitStart"));
            visitEnd.ifPresent(value -> filters.add(" visit.endDate < :visitEnd"));

            if (!filters.isEmpty()) {
                baseQuery.append(" and ").append(String.join(" and ", filters));
            }

            sortOptions.ifPresent(value -> baseQuery.append(value.getQuery()));

            Query<User> query = session.createQuery(baseQuery.toString(), User.class);

            if (visitStart.isPresent()) {
                Date startDate = parseOptionalStringToLocalDateTime(visitStart);
                params.put("visitStart", startDate);
            }

            if (visitEnd.isPresent()) {
                Date endDate = parseOptionalStringToLocalDateTime(visitEnd);
                params.put("visitEnd", endDate);
            }
            query.setProperties(params);
            return query.list();
        }
    }

    public static Date parseOptionalStringToLocalDateTime(Optional<String> date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        String fromDate = date.get();
        return (Date)formatter.parse(fromDate);
    }

}

