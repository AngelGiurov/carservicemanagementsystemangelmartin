package com.smartgarage.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PassRestartRequestDto {
    public static final String EMAIL_LENGTH_RESTRICTION = "Email length restriction";
    @NotNull
    @Email
    @Size(min = 5, max = 40, message = EMAIL_LENGTH_RESTRICTION)
    private String email;

    public PassRestartRequestDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
