package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.AuthenticationFailureException;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Make;
import com.smartgarage.models.User;
import com.smartgarage.models.Vehicle;
import com.smartgarage.models.dtos.*;
import com.smartgarage.models.mappers.ModelMapper;
import com.smartgarage.models.mappers.VehicleMapper;
import com.smartgarage.services.contracts.MakeService;
import com.smartgarage.services.contracts.ModelService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehiclesMvcController {

    private final VehicleService vehicleService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final ModelService modelService;
    private final MakeService makeService;
    private final VehicleMapper vehicleMapper;

    public VehiclesMvcController(VehicleService vehicleService,
                                 AuthenticationHelper authenticationHelper, UserService userService, ModelMapper modelMapper, ModelService modelService, MakeService makeService, VehicleMapper vehicleMapper) {
        this.vehicleService = vehicleService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
        this.makeService = makeService;
        this.vehicleMapper = vehicleMapper;
    }

    @ModelAttribute("allVehicles")
    public List<Vehicle> populateVehicles(HttpSession session) {
        return vehicleService.getAll();
    }

    @ModelAttribute("allMakes")
    public List<Make> populateMakes(HttpSession session) {
        return makeService.getAll();
    }

    @ModelAttribute("allModels")
    public List<com.smartgarage.models.Model> populateModels(HttpSession session) {
        return modelService.getAll();
    }

    @GetMapping
    public String showAllVehicles(Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        List<User> users = userService.getAll();
        model.addAttribute("currentUser",currentUser);
        model.addAttribute("users", users);
        model.addAttribute("user", new User());
        model.addAttribute("editProfileDto",new EditProfileDto());
        model.addAttribute("editVehicleDto", new EditVehicleDto());
        model.addAttribute("visitDto", new VisitDTO());
        model.addAttribute("modelDto", new ModelDTO());
        model.addAttribute("brandDto", new BrandDTO());
        model.addAttribute("make", new Make());
        model.addAttribute("model", new com.smartgarage.models.Model());
        model.addAttribute("vehicleDto", new VehicleDTO());
        return "vehicles";
    }


    @PostMapping("/{id}/delete")
    public String deleteService(@Valid
                                @ModelAttribute("vehicle")
                                        Vehicle vehicle,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            vehicleService.delete(vehicle.getId(), user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }

        return "redirect:/vehicles";
    }

    @PostMapping("/{id}/addUser")
    public String addUserToVehicle(@PathVariable int id, @Valid
                                @ModelAttribute("user")
                                        User user,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User authorizeUser;
        try {
            authorizeUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            Vehicle vehicle = vehicleService.getById(id);
            vehicleService.addUser(vehicle, user, authorizeUser);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }
        return "redirect:/vehicles";
    }

    @PostMapping("/model")
    public String registerModel(@Valid @ModelAttribute("modelDto")
                                        ModelDTO modelDto,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User authorizeUser;
        try {
            authorizeUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            com.smartgarage.models.Model modelToCreate = modelMapper.fromDto(modelDto);
            modelService.createModel(authorizeUser, modelToCreate);
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("make_id", "auth_error", e.getMessage());
            return "redirect:/vehicles";
        }
    }

    @PostMapping("/make")
    public String registerMake(@Valid @ModelAttribute("brandDto")
                                        BrandDTO brandDTO,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User authorizeUser;
        try {
            authorizeUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            makeService.createMake(authorizeUser, brandDTO.getName());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("make_id", "auth_error", e.getMessage());
            return "redirect:/vehicles";
        }
    }

    @PostMapping("/modelUpdate")
    public String updateModel(@Valid
                                @ModelAttribute("model")
                              com.smartgarage.models.Model model,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            modelService.updateModel(user, model);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }
    }

    @PostMapping("/makeUpdate")
    public String updateMake(@Valid
                              @ModelAttribute("make")
                                      Make make,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            makeService.updateMake(user, make);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }
    }

    @PostMapping("/{id}/edit")
    public String updateVehicle(@PathVariable int id,@Valid
                                @ModelAttribute("editVehicleDto")
                                        EditVehicleDto editVehicleDto,
                                BindingResult bindingResult,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        try {
            Vehicle vehicleToUpdate = vehicleMapper.fromDto(editVehicleDto, id);
            vehicleService.update(user, vehicleToUpdate);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }
    }

    @PostMapping("/create")
    public String addVehicle(@Valid
    @ModelAttribute("vehicleDto")
            VehicleDTO vehicleDTO,
                                BindingResult bindingResult,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/vehicles";
        }
        try {
            Vehicle vehicle = vehicleMapper.fromDto(vehicleDTO);
            vehicleService.create(vehicle, user);
            return "redirect:/vehicles";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
            return "redirect:/vehicles";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/Login";
        }
    }

}
