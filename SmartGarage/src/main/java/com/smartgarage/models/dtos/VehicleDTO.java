package com.smartgarage.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;

public class VehicleDTO {

    public static final String INVALID_PLATE = "Valid bulgarian license plates contain between 7 and 8 characters";
    public static final String INVALID_VIN = "Valid VIN must be 17 characters long";
    @JsonProperty(value = "VIN")
    @Size(min = 17, max = 17, message = INVALID_VIN)
    private String VIN;

    @JsonProperty(value = "licensePlate")
    @Size(min = 7, max = 8, message = INVALID_PLATE)
    private String licensePlate;

    @JsonProperty(value = "creationYear")
    private int creationYear;

    @JsonProperty(value = "model")
    private int modelId;

    @JsonProperty(value = "user")
    private int userId;

    public VehicleDTO() {
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getCreationYear() {
        return creationYear;
    }

    public void setCreationYear(int creationYear) {
        this.creationYear = creationYear;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }
}
