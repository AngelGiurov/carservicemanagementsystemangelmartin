package com.smartgarage.controllers.mvc;

import com.smartgarage.controllers.AuthenticationHelper;
import com.smartgarage.exceptions.AuthenticationFailureException;
import com.smartgarage.exceptions.DuplicateEntityException;
import com.smartgarage.exceptions.EntityNotFoundException;
import com.smartgarage.exceptions.UnauthorizedOperationException;
import com.smartgarage.models.Currency;
import com.smartgarage.models.Service;
import com.smartgarage.models.User;
import com.smartgarage.models.Visit;
import com.smartgarage.models.dtos.*;
import com.smartgarage.models.mappers.UserMapper;
import com.smartgarage.models.mappers.VisitMapper;
import com.smartgarage.repositories.contracts.CurrencyJpaRepository;
import com.smartgarage.services.contracts.ServicesService;
import com.smartgarage.services.contracts.UserService;
import com.smartgarage.services.contracts.VehicleService;
import com.smartgarage.services.contracts.VisitService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/my-profile")
public class UserProfileMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ServicesService servicesService;
    private final UserMapper userMapper;
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final VehicleService vehicleService;
    private final CurrencyJpaRepository currencyJpaRepository;

    public UserProfileMvcController(AuthenticationHelper authenticationHelper, UserService userService, ServicesService servicesService, UserMapper userMapper, VisitService visitService, VisitMapper visitMapper, VehicleService vehicleService, CurrencyJpaRepository currencyJpaRepository) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.servicesService = servicesService;
        this.userMapper = userMapper;
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.vehicleService = vehicleService;
        this.currencyJpaRepository = currencyJpaRepository;
    }

    @ModelAttribute("vehicleServices")
    public List<Service> populateVehicleServices(HttpSession session) {
        return servicesService.getAll();
    }

    @ModelAttribute("allCurrencies")
    public List<Currency> populateAllCurrencies(HttpSession session) {
        return currencyJpaRepository.findAll();
    }

    @GetMapping("")
    public String showUserPage(Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("services", servicesService.getServicesByUser(currentUser));
            model.addAttribute("visits", visitService.getByUser(currentUser));
            model.addAttribute("vehicles", vehicleService.getByOwner(currentUser.getId()));

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        model.addAttribute("passwordChangeDto", new PasswordChangeDto());
        model.addAttribute("editProfileDto", new EditProfileDto());
        model.addAttribute("visitReportDto", new VisitReportDto());
        model.addAttribute("filterServicesDto", new FilterServicesDTO());
        model.addAttribute("visitDto", new VisitDTO());
        model.addAttribute("service", new Service());
        return "my-profile";

    }


    @PostMapping("/user-pass-change")
    public String userPassChange(@Valid @ModelAttribute("passwordChangeDTO")
                                         PasswordChangeDto passwordChangeDto,
                                 BindingResult bindingResult, HttpSession session,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User userToConfirmAndChange;
        try {
            userToConfirmAndChange = authenticationHelper.tryGetUser(session);
            ;
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            model.addAttribute("passwordChangeDto", new PasswordChangeDto());
            model.addAttribute("editProfileDto", new EditProfileDto());
            model.addAttribute("visitReportDto", new VisitReportDto());
            model.addAttribute("filterServicesDto", new FilterServicesDTO());
            userService.changePassword(userToConfirmAndChange, passwordChangeDto);
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("oldPassword", "auth_error", e.getMessage());
            return "redirect:/my-profile";
        }
        return "redirect:/my-profile";
    }

    @PostMapping("/user-info-change")
    public String userInfoChange(@Valid @ModelAttribute("editProfileDto")
                                         EditProfileDto editProfileDto,
                                 Model model,
                                 BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User authorizedUser;
        try {
            authorizedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            model.addAttribute("passwordChangeDto", new PasswordChangeDto());
            model.addAttribute("editProfileDto", new EditProfileDto());
            model.addAttribute("visitReportDto", new VisitReportDto());
            model.addAttribute("filterServicesDto", new FilterServicesDTO());
            User userToBeUpdated = userMapper.fromDto(editProfileDto, authorizedUser.getId());
            userService.update(authorizedUser, userToBeUpdated);
        } catch (AuthenticationFailureException e) {

            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "redirect:/my-profile";

        } catch (UnauthorizedOperationException e) {
            return "unauthorized-access";
        }
        return "redirect:/my-profile";
    }

    @PostMapping("/filter")
    public String filterPosts(@ModelAttribute("filterServicesDto") FilterServicesDTO filterServicesDTO,
                              BindingResult bindingResult, Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            model.addAttribute("currentUser", user);
            model.addAttribute("passwordChangeDto", new PasswordChangeDto());
            model.addAttribute("editProfileDto", new EditProfileDto());
            model.addAttribute("visitReportDto", new VisitReportDto());
            model.addAttribute("filterServicesDto", new FilterServicesDTO());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        var filtered = servicesService.filterByPrice(
                user,
                Optional.ofNullable(filterServicesDTO.getPrice() == null ? null : filterServicesDTO.getPrice()),
                Optional.ofNullable(filterServicesDTO.getName().isBlank() ? null : filterServicesDTO.getName()));
        model.addAttribute("services", filtered);
        return "my-profile";
    }

    @PostMapping("/sendReport")
    public String sendReport(@Valid @ModelAttribute("visitReportDto")
                                     VisitReportDto visitReportDto,
                             Model model,
                             BindingResult bindingResult,
                             HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }

        try {
            visitService.sendReportEmail(visitReportDto.getId(), visitReportDto.getCurrencyId(), user);
        } catch (UnauthorizedOperationException |
                AuthenticationFailureException |
                EntityNotFoundException e) {
            bindingResult.rejectValue("visitId", "auth_error", e.getMessage());
            return "redirect:/my-profile";
        }

        return "redirect:/my-profile";
    }

    @PostMapping("/visit")
    public String handleRegisterVisit(@Valid @ModelAttribute("visitDto")
                                              VisitDTO visitDto,
                                      BindingResult bindingResult,
                                      HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            Visit visitToCreate = visitMapper.fromDto(visitDto);
            visitService.create(visitToCreate, user);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "redirect:/my-profile";
    }

    @PostMapping("/serviceUpdate")
    public String updateService(@Valid
                                @ModelAttribute("service")
                                        Service service,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            servicesService.update(service, user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/my-profile";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        }

        return "redirect:/my-profile";
    }

    @PostMapping("/serviceDelete")
    public String deleteService(@Valid
                                @ModelAttribute("service")
                                        Service service,
                                BindingResult bindingResult,
                                HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/Login";
        }
        try {
            servicesService.delete(service.getId(), user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("service", "serviceUpdate", e.getMessage());
            return "redirect:/my-profile";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/unauthorized-access";
        }

        return "redirect:/my-profile";
    }

}
